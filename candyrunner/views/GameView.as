package candyrunner.views 
{
	import candyrunner.core.HitTestManager;
	import candyrunner.core.IUpdateableObject;
	import candyrunner.core.IView;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import candyrunner.level.LevelManager;
	import candyrunner.level.ObstacleManager;
	import candyrunner.loader.Components;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import candyrunner.main.SceneEvent;
	import candyrunner.paralax.ParalaxManager;
	import candyrunner.paralax.PowerManager;
	import candyrunner.player.Hero;
	import candyrunner.player.HeroController;
	import candyrunner.player.HeroScore;
	import candyrunner.screens.SceneManager;
	import candyrunner.sound.SoundAssets;
	import candyrunner.sound.SoundManager;
	import candyrunner.ui.HUD;
	import flash.display.Bitmap;
	import flash.media.Sound;
	import flash.net.SharedObject;
	import starling.display.Sprite;
	import flash.system.System;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class GameView extends Sprite implements IView
	{		
		private var gameAtlas:TextureAtlas;
		
		private var paralaxManager:ParalaxManager;
		private var obstacleManager:ObstacleManager;
		private var hero:Hero;
		private var heroController:HeroController;
		private var levelManager:LevelManager;
		private var hitTestManager:HitTestManager;
		private var powerManager:PowerManager;
		
		private var dispatcher:GameDispatcher
		private var bitmap:Object;
		private var xml:Object;		
		
		private var gameElements:Vector.<IUpdateableObject>;
		private var stopFlag:Boolean;
		
		
		public function GameView() 
		{
			super()
			dispatcher = GameDispatcher.instance;
			dispatcher.addEventListener(GameEvent.ATLAS_FOR_GAME, onAtlas);
		}
		
		private function onAtlas(e:GameEvent):void 
		{
			gameAtlas = TextureAtlas(e.data);
			dispatcher.dispatchEvent(new SceneEvent(null, ViewConfig.GAME_VIEW));
		}
		
		public function init():void
		{
			randomizeMode();
			gameElements = new Vector.<IUpdateableObject>();
			
			levelManager = new LevelManager(gameAtlas);
			var heroVector:Vector.<Texture> = new Vector.<Texture>();
			
			for each (var element:String in TextureData.PLAYER) 
			{
				heroVector.push(gameAtlas.getTexture(element));
			}
			
			hero = new Hero(heroVector);
			
			paralaxManager = new ParalaxManager(levelManager.paralaxVector);
			hitTestManager = new HitTestManager(hero);
			powerManager = new PowerManager(hero);
			obstacleManager = new ObstacleManager(gameAtlas, levelManager, hitTestManager);
			
			addChild(levelManager);
			addChild(hero);
			addChild(hitTestManager.debugSprite);
			
			addElement(hero);
			addElement(paralaxManager);
			addElement(powerManager);
			addElement(obstacleManager);
			
			heroController = new HeroController(hero);
			
			dispatcher.addEventListener(GameEvent.END_GAME, onEndHandler);
			dispatcher.addEventListener(GameEvent.PAUSE, onPauseHandler);
			dispatcher.addEventListener(GameEvent.UNPAUSE, onUnPauseHandler);
			this.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
			
			GameDispatcher.instance.dispatchEventWith("PLAY_BACKGROUND", false, GameData.GAME_BACKGROUND);
			dispatcher.dispatchEvent(new GameEvent(GameEvent.REMOVE_PRELOADER));
		}
		
		private function randomizeMode():void 
		{
			if (Math.random() > 0.5) 
			{
				trace("EVIL");
				GameData.GAME_MODE_EVIL = true;
			}
			else 
			{
				trace("GOOD");
				GameData.GAME_MODE_EVIL = false;
			}
		}	
		
		public function clean():void
		{
			this.removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
			
			trace("CLEAN GAME");
			if (heroController != null) 
			{
				heroController.removeMe();
			}
			hero.removeMe();
			
			obstacleManager.removeMe();
			powerManager.removeMe();
			hitTestManager.removeMe();				
			paralaxManager.removeMe();
			levelManager.removeMe();
			
			removeElement(paralaxManager);
			removeElement(powerManager);
			removeElement(hero);
			removeElement(obstacleManager);
			
			removeChild(hero);
			removeChild(hitTestManager.debugSprite);
			removeChild(levelManager);
			
			powerManager = null;
			obstacleManager = null;
			hitTestManager = null;
			paralaxManager = null;
			
			heroController = null;
			hero = null;
			levelManager = null;
			
			stopFlag = false;
			
			gameAtlas.dispose();
			gameAtlas = null;
			
			System.gc();
		}		
		
		public function addElement(element:IUpdateableObject):void 
		{
			gameElements.push(element);
		}
		
		public function removeElement(element:IUpdateableObject):void
		{
			var index:int = gameElements.indexOf(element);
			if (index != -1) 
			{
				trace("Success to remove: ", element);
				gameElements[index] = null;
				gameElements.splice(index, 1);
			}
			else 
			{
				trace("Fail to remove: ", element);
			}
		}		
		
		private function onPauseHandler(e:Event):void 
		{
			if (this.hasEventListener(EnterFrameEvent.ENTER_FRAME))
			{
				trace("pause");
				paralaxManager.pauseMultiply();
				this.removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
				if (hero.ghostModeTimer)
					hero.ghostModeTimer.stop();
			}
		}
		
		private function onUnPauseHandler(e:GameEvent):void 
		{
			if (!this.hasEventListener(EnterFrameEvent.ENTER_FRAME))
			{				
				trace("unpause");
				this.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
				paralaxManager.startMultiply();
				if (hero.ghostModeTimer)
					hero.ghostModeTimer.start();
			}
		}
		
		private function onEndHandler(e:Event):void 
		{
			hero.removeEventListeners();
			HeroScore.saveHighscores();
		}
		
		private function onEnterFrameHandler():void
		{
			GameData.RADIAN++;
			
			for each(var element:IUpdateableObject in gameElements)
			{
				if (element != null) 
				{
					element.update();
				}
			}
			if (hitTestManager != null)
			{
				hitTestManager.checkCollision(hero);
				//hitTestManager.debugDraw();
			}
			
			dispatcher.dispatchEvent(new GameEvent(GameEvent.UPDATE_HUD));
		}
	}	
}
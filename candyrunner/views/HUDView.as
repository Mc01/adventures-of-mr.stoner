package candyrunner.views 
{
	import candyrunner.core.IView;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import candyrunner.main.SceneEvent;
	import candyrunner.screens.SceneManager;
	import candyrunner.ui.HUD;
	import flash.display.Bitmap;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	
	public class HUDView extends Sprite implements IView
	{	
		private var hudAtlas:TextureAtlas;
		private var hud:HUD;
		
		public function HUDView() 
		{
			super();
			GameDispatcher.instance.addEventListener(GameEvent.ATLAS_FOR_GAME, onAtlas);
		}
		
		private function onAtlas(e:Event):void 
		{
			hudAtlas = TextureAtlas(e.data);
			GameDispatcher.instance.dispatchEvent(new SceneEvent(null, ViewConfig.HUD_VIEW));
		}
		
		public function init():void
		{
			hud = new HUD(hudAtlas);
			addChild(hud);
		}
		
		public function clean():void
		{
			hudAtlas = null;
			removeChildren();
			if (hud != null) 
			{
				hud.removeMe();
			}
			hud = null;
		}
	}
}
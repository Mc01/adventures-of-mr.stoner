package candyrunner.views 
{
	import candyrunner.core.IView;
	import candyrunner.data.GameData;
	import candyrunner.loader.Components;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import candyrunner.main.SceneEvent;
	import candyrunner.screens.HelpScreen;
	import candyrunner.screens.MenuScreen;
	import candyrunner.screens.OptionScreen;
	import candyrunner.screens.SceneManager;
	import candyrunner.screens.ScoreScreen;
	import candyrunner.screens.ScreenConfig;
	import flash.display.Bitmap;
	import starling.events.Event;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	
	public class MenuView extends Sprite implements IView
	{	
		private var menuAtlas:TextureAtlas;
		private var dispatcher:GameDispatcher;
		private var menuScreen:MenuScreen;
		private var optionScreen:OptionScreen;
		private var scoreScreen:ScoreScreen;
		private var helpScreen:HelpScreen;
		
		public function MenuView() 
		{
			super();
			dispatcher = GameDispatcher.instance;	
			dispatcher.addEventListener(GameEvent.ATLAS_FOR_MENU, onAtlas);
			dispatcher.addEventListener(GameEvent.CHANGE_SCREEN, onChangeScreen);
		}
		
		private function onAtlas(e:GameEvent):void 
		{
			menuAtlas = TextureAtlas(e.data);
			dispatcher.dispatchEvent(new SceneEvent(null, ViewConfig.MENU_VIEW));
			GameDispatcher.instance.dispatchEventWith("PLAY_BACKGROUND", false, GameData.MENU_BACKGROUND);
		}
		
		private function onChangeScreen(e:GameEvent):void 
		{
			var screen:String = String(e.data);
			GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, GameData.CLICK);			
			trace(screen);
			
			switch (screen) 
			{
				case ScreenConfig.MENU_TO_OPTION:
					menuScreen.disable();
					optionScreen.enable();
				break;
				case ScreenConfig.MENU_TO_SCORES:
					menuScreen.disable();
					scoreScreen.enable();
				break;
				case ScreenConfig.MENU_TO_HELP:
					menuScreen.disable();
					helpScreen.enable();
				break;
				case ScreenConfig.OPTION_TO_MENU:
					optionScreen.disable();
					menuScreen.enable();
				break;
				case ScreenConfig.SCORES_TO_MENU:
					scoreScreen.disable();
					menuScreen.enable();
				break;
				case ScreenConfig.HELP_TO_MENU:
					helpScreen.disable();
					menuScreen.enable();
				break;
			}
		}
		
		public function init():void
		{
			menuScreen = new MenuScreen(menuAtlas);
			addChild(menuScreen);
			
			optionScreen = new OptionScreen(menuAtlas);
			optionScreen.disable();
			addChild(optionScreen);
			
			scoreScreen = new ScoreScreen(menuAtlas);
			scoreScreen.disable();
			addChild(scoreScreen);
			
			helpScreen = new HelpScreen(menuAtlas);
			helpScreen.disable();
			addChild(helpScreen);
			
			dispatcher.dispatchEvent(new GameEvent(GameEvent.REMOVE_PRELOADER));
		}
		
		
		public function clean():void
		{
			menuAtlas.dispose();
			menuAtlas = null;
			helpScreen.removeMe();
			scoreScreen.removeMe();
			optionScreen.removeMe();
			menuScreen.removeMe();
			removeChild(helpScreen);
			removeChild(scoreScreen);
			removeChild(optionScreen);
			removeChild(menuScreen);
			helpScreen = null;
			scoreScreen = null;
			optionScreen = null;
			menuScreen = null;
		}
	}
}
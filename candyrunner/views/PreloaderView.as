package candyrunner.views 
{
	import candyrunner.assets.TextureCache;
	import candyrunner.core.IView;
	import candyrunner.data.GameData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.ui.PreloaderGuy;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.text.TextField;
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.QuadBatch;
	import starling.display.Sprite;
	import Preloader.GamePreloader
	import starling.events.EnterFrameEvent;
	import starling.textures.Texture;
	
	
	public class PreloaderView extends Sprite implements IView
	{
		
		private var quad:Quad;
		private var _dispatcher:GameDispatcher
		private var bar:Sprite;
		private var barBackground:Sprite;
		private var background:Quad;
		private var preloaderGuy:PreloaderGuy;
		private var loading:Sprite;
		
		public function PreloaderView() 
		{
			_dispatcher = GameDispatcher.instance;
			_dispatcher.addEventListener('UPDATE_BAR', onUpdate);
		}
		
		private function onUpdate(e:starling.events.Event):void 
		{
			//trace(bar.width);
			var normalSize:Number = (GameData.BYTES_LOADED / GameData.BYTES_TO_LOAD)
			if (isNaN(normalSize))
				normalSize = 0;
			//bar.width = GameData.FILES_LOADED/GameData.FILES_TO_LOAD * barBackground.width + 1/GameData.FILES_TO_LOAD * normalSize * barBackground.width;
			preloaderGuy.changeHeight(GameData.FILES_LOADED / GameData.FILES_TO_LOAD * barBackground.height + 1 / GameData.FILES_TO_LOAD * normalSize * barBackground.height);
			loading.y = 670 + 10 * Math.sin(720 * normalSize * Math.PI / 180);
		}
		
		public function init():void
		{
			var preloader:GamePreloader = new GamePreloader();			
			
			background = new Quad(1024, 768, 0x000000);
			background.x = background.y = 0;
			addChild(background);
			
			barBackground = getSpriteFromSWC(preloader, "BarBackground");
			barBackground.x = 350;
			barBackground.y = 50;
			addChild(barBackground);
			
			loading = getSpriteFromSWC(preloader, "Loading");
			loading.x = 362;
			loading.y = 670;
			addChild(loading);
			
			preloaderGuy = new PreloaderGuy(350, 50);
			addChild(preloaderGuy);
		}
		
		public function clean():void
		{
			removeChild(quad);
			quad = null;
		}
		
		public function minimizeBar():void
		{
			preloaderGuy.changeHeight(603);
		}
		
		private function getSpriteFromSWC(swcContent:Object, childName:String):Sprite
		{
			var helperSprite:flash.display.Sprite = swcContent.getChildByName(childName) as flash.display.Sprite;
			var bitmapData:BitmapData = new BitmapData(helperSprite.width, helperSprite.height, true, 0x00FFFFFF);
			bitmapData.draw(helperSprite);
			var bitmap:Bitmap = new Bitmap(bitmapData, "auto", false)
			
			var texture:Texture = Texture.fromBitmap(Bitmap(bitmap));
			var objectImage:Image = new Image(texture);
			var sprite:Sprite = new Sprite();
			sprite.addChild(objectImage);
			return sprite;
		}

		
	}

}
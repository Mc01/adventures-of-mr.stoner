package candyrunner.views 
{
	import candyrunner.core.IView;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.ui.Countdown;
	import starling.display.Sprite;
	import starling.textures.TextureAtlas;
	
	public class FadeView extends Sprite implements IView 
	{
		private var fadeAtlas:TextureAtlas;
		private var countdown:Countdown;
		
		public function FadeView() 
		{
			super();
			GameDispatcher.instance.addEventListener(GameEvent.ATLAS_FOR_GAME, onAtlas);
		}
		
		private function onAtlas(e:GameEvent):void 
		{
			fadeAtlas = TextureAtlas(e.data);
		}
		
		/* INTERFACE candyrunner.core.IView */
		
		public function init():void 
		{
			countdown = new Countdown(fadeAtlas);
			addChild(countdown);
			countdown.startCountdown();
		}
		
		public function clean():void 
		{
			removeChildren();
			countdown.removeMe();
			countdown = null;
			this.dispose();
		}
	}
}
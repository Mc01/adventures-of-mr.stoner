package candyrunner.views 
{
	import candyrunner.core.IView;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.ui.Popup;
	import flash.display.Bitmap;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class PopupView extends Sprite implements IView
	{
		private var popupAtlas:TextureAtlas;
		private var popup:Popup;
		
		public function PopupView() 
		{
			super();
			GameDispatcher.instance.addEventListener(GameEvent.ATLAS_FOR_GAME, onAtlas);
		}
		
		private function onAtlas(e:GameEvent):void 
		{
			popupAtlas = TextureAtlas(e.data);
		}
		
		/* INTERFACE candyrunner.core.IView */
		
		public function init():void 
		{
			popup = new Popup(popupAtlas);
			addChild(popup);
		}
		
		public function clean():void 
		{
			removeChildren();
			popup.removeMe();
			popup = null;
			this.dispose();
		}
	}
}
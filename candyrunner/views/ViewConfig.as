package candyrunner.views 
{
	public class ViewConfig 
	{
		public static const FONTS:String = "Fonts";
		public static const PRELOADER_VIEW:String = "PreloaderView";
		public static const MENU_VIEW:String = "MenuView";
		public static const GAME_VIEW:String = "GameView";
		public static const HUD_VIEW:String = "HUDView";
		public static const FADE_VIEW:String = "FadeView";
		public static const POPUP_VIEW:String = "Popup";
		public static const INFO_VIEW:String = "InfoView";
	}
}
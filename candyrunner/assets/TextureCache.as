package candyrunner.assets 
{
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class TextureCache 
	{
		private var _textures:Vector.<Texture>;
		
		public function TextureCache(atlas:TextureAtlas, names:Array) 
		{
			_textures = new Vector.<Texture>();
			for (var i:int = 0; i < names.length; i++) 
			{
				_textures.push(atlas.getTexture(names[i]));
			}
		}
		
		public function removeMe():void 
		{
			for (var i:int = 0; i < _textures.length; i++) 
			{
				_textures[i] = null;
			}
			_textures = null;
		}
		
		public function get textures():Vector.<Texture> 
		{
			return _textures;
		}
	}
}
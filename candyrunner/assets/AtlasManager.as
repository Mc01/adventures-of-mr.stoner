package candyrunner.assets 
{
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.SceneEvent;
	import candyrunner.views.ViewConfig;
	import flash.display.Bitmap;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class AtlasManager 
	{
		private var dispatcher:GameDispatcher;
		private var bitmap:Bitmap;
		private var xml:XML;
		
		public function AtlasManager() 
		{
			dispatcher = GameDispatcher.instance;
			dispatcher.addEventListener(ViewConfig.MENU_VIEW, onMenuLoaded);
			dispatcher.addEventListener(ViewConfig.GAME_VIEW, onGameLoaded);
			dispatcher.addEventListener(GameEvent.ONCE_AGAIN_ATLAS, onAgain);
		}
		
		private function onAgain(e:GameEvent):void 
		{
			var atlas:TextureAtlas = new TextureAtlas(Texture.fromBitmap(bitmap), xml);
			dispatcher.dispatchEvent(new GameEvent(GameEvent.ATLAS_FOR_GAME, atlas));
		}
		
		private function onMenuLoaded(e:Event):void 
		{
			var array:Array = e.data as Array;
			bitmap = Bitmap(array[0]);
			xml = XML(array[1]);
			var atlas:TextureAtlas = new TextureAtlas(Texture.fromBitmap(bitmap), xml);
			dispatcher.dispatchEvent(new GameEvent(GameEvent.ATLAS_FOR_MENU, atlas));
		}
		
		private function onGameLoaded(e:Event):void 
		{
			var array:Array = e.data as Array;
			bitmap = Bitmap(array[0]);
			xml = XML(array[1]);
			var atlas:TextureAtlas = new TextureAtlas(Texture.fromBitmap(bitmap), xml);
			dispatcher.dispatchEvent(new GameEvent(GameEvent.ATLAS_FOR_GAME, atlas));
		}
	}
}
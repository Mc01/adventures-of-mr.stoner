package candyrunner.assets 
{
	public class GameAssets 
	{		
		
		[Embed(source = "/../assets/game_font.fnt", mimeType = "application/octet-stream")]
		public static const FontXML:Class;
		
		[Embed(source = "/../assets/game_font.png")]
		public static const FontTexture:Class;
	}
}
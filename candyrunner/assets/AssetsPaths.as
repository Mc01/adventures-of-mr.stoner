package candyrunner.assets 
{
	public class AssetsPaths 
	{
		public static const GAME_PNG:String = "assets/gameAtlas.png";
		public static const GAME_XML:String = "assets/gameAtlas.xml";
		public static const FONT_PNG:String = "assets/rustler.png";
		public static const FONT_FNT:String = "assets/rustler.fnt";
		public static const MENU_PNG:String = "assets/menuAtlas.png";
		public static const MENU_XML:String = "assets/menuAtlas.xml";		
	}
}
package candyrunner
{
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameManager;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import starling.core.Starling;
	import Preloader.GamePreloader;
	
	
	
	[Frame(factoryClass="candyrunner.Preloader")]
	[SWF(frameRate="60", width="1024", height="768", backgroundColor = "0x000000")]
	public class Main extends Sprite 
	{
		
		private var _gamepreloader:GamePreloader;
		private var _starling:Starling;
		
		public function Main(gamePreloader:GamePreloader):void 
		{
			super();
			_gamepreloader = gamePreloader;
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);			
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			Starling.multitouchEnabled = true;
			Starling.handleLostContext = true;
			
			_starling = new Starling(GameManager, stage, new Rectangle(0, 0, stage.stageWidth, stage.stageHeight));
			_starling.showStats = true;
			
			_starling.start();
			_starling.addEventListener('rootCreated', onRootCreatedHandler);
		}
		
		private function onRootCreatedHandler(e:Object):void 
		{
			_gamepreloader.visible = false;
		}

	}
}
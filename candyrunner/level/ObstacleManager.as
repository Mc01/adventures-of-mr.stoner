package candyrunner.level 
{
	import candyrunner.core.GameObject;
	import candyrunner.core.HitTestManager;
	import candyrunner.core.IHitTestObject;
	import candyrunner.core.ILayer;
	import candyrunner.core.IUpdateableObject;
	import candyrunner.data.GameData;
	import candyrunner.data.PlayerData;
	import candyrunner.data.PositionData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameManager;
	import starling.display.Sprite;
	import starling.textures.TextureAtlas;
	
	public class ObstacleManager implements IUpdateableObject, ILayer
	{
		private var atlas:TextureAtlas;
		private var levelManager:LevelManager;
		private var hitManager:HitTestManager;
		
		private var smallPool:ObstaclePool;
		private var bigPool:ObstaclePool;
		private var coinPool:ObstaclePool;
		
		private var smallEvilFrom:int;
		private var smallEvilTo:int;
		private var smallGoodFrom:int;
		private var smallGoodTo:int;
		private var bigEvilFrom:int;
		private var bigEvilTo:int;
		private var bigGoodFrom:int;
		private var bigGoodTo:int;
		
		private var distance:Number;
		private var between:int;
		private var spawnPoint:int;
		private var coinNumb:int;
		private var coinDiff:int;
		private var coinPoint:int;
		private var bPathIsUp:Boolean;
		private const OPTION_NO_FORCE:int = 0;
		private const OPTION_FORCE_TOP:int = 1;
		private const OPTION_FORCE_BOTTOM:int = 2;
		private var powerPool:ObstaclePool;
		
		public function ObstacleManager(atlas:TextureAtlas, levelManager:LevelManager, hitManager:HitTestManager) 
		{
			this.atlas = atlas;
			this.levelManager = levelManager;
			this.hitManager	= hitManager;
			init();
		}
		
		private function init():void 
		{
			initVars();
			initPools();
			initPath();
		}
		
		private function initVars():void 
		{
			distance = 0;
			between = GameData.OBSTACLE_BETWEEN;
			spawnPoint = PositionData.X_SECOND_SCREEN;
			coinNumb = GameData.OBSTACLE_COIN_NUMB;
			coinDiff = GameData.OBSTACLE_COIN_DIFF;
			coinPoint = spawnPoint - (coinNumb / 2) * coinDiff;
		}
		
		private function initPools():void 
		{
			var smallArray:Array = new Array().concat(TextureData.OBSTACLES_SMALL_EVIL, TextureData.OBSTACLES_SMALL_GOOD);
			var bigArray:Array = new Array().concat(TextureData.OBSTACLES_BIG_EVIL, TextureData.OBSTACLES_BIG_GOOD);
			var coinArray:Array = new Array().concat(TextureData.OBSTACLES_COINS);
			
			smallEvilFrom = 0;
			smallEvilTo = TextureData.OBSTACLES_SMALL_EVIL.length;
			smallGoodFrom = smallEvilTo;
			smallGoodTo = smallArray.length;
			
			bigEvilFrom = 0;
			bigEvilTo = TextureData.OBSTACLES_BIG_EVIL.length;
			bigGoodFrom = bigEvilTo
			bigGoodTo = bigArray.length;
			
			smallPool = new ObstaclePool(atlas, smallArray, 10, GameData.OBSTACLE_TYPE_HARMFUL);
			bigPool = new ObstaclePool(atlas, bigArray, 5, GameData.OBSTACLE_TYPE_HARMFUL);
			coinPool = new ObstaclePool(atlas, coinArray, 50, GameData.OBSTACLE_TYPE_COIN);
			powerPool = new ObstaclePool(atlas, coinArray, 5, GameData.OBSTACLE_TYPE_POWER);
		}
		
		private function initPath():void 
		{
			if (Math.random() > 0.5) 
			{
				bPathIsUp = true;
			}
			else 
			{
				bPathIsUp = false;
			}
		}	
		
		/* INTERFACE candyrunner.core.IUpdateableObject */
		
		public function update():void 
		{
			distance += GameData.OVERALL_SPEED;
			if (distance > between) 
			{
				distance = 0;
				
				var spawn:Vector.<Obstacle> = new Vector.<Obstacle>();
				var random:int = Math.random() * 100;
				var coin:Obstacle;
				
				distortPath(random);
				spawnObstacles(spawn, random);
				spawnCoins(spawn, random);				
				createCollisions(spawn);
			}
		}		
		
		private function distortPath(random:int):void 
		{
			var distort:int = Math.random() * 100;
			if (distort < random) 
			{
				togglePath();
			}
		}
		
		private function spawnObstacles(spawn:Vector.<Obstacle>, random:int):void 
		{			
			if (random < 10) 
			{
				return;
			}
			else if (random < 20) 
			{	
				spawn.push(smallPool.checkOut(smallEvilFrom, smallEvilTo));
				spawn.push(smallPool.checkOut(smallGoodFrom, smallGoodTo));
				if (GameData.GAME_MODE_EVIL) //array.concat(EVIL, GOOD)
				{
					spawnObstacleBottom(spawn[0]);
					spawnObstacleTop(spawn[1]);
				}
				else 
				{
					spawnObstacleTop(spawn[0]);
					spawnObstacleBottom(spawn[1]);
				}					
			}
			else if (random < 35) 
			{					
				if (GameData.GAME_MODE_EVIL) //array.concat(EVIL, GOOD)
				{
					spawn.push(bigPool.checkOut(bigEvilFrom, bigEvilTo));
				}
				else 
				{
					spawn.push(bigPool.checkOut(bigGoodFrom, bigGoodTo));
				}
				spawnObstacleBottom(spawn[0]);
			}
			else if (random < 50) 
			{
				if (GameData.GAME_MODE_EVIL) //array.concat(EVIL, GOOD)
				{
					spawn.push(bigPool.checkOut(bigGoodFrom, bigGoodTo));
				}
				else 
				{
					spawn.push(bigPool.checkOut(bigEvilFrom, bigEvilTo));
				}
				spawnObstacleTop(spawn[0]);
			}
			else if (random < 75) 
			{
				if (GameData.GAME_MODE_EVIL) //array.concat(EVIL, GOOD)
				{
					spawn.push(smallPool.checkOut(smallEvilFrom, smallEvilTo));
				}
				else 
				{
					spawn.push(smallPool.checkOut(smallGoodFrom, smallGoodTo));
				}
				spawnObstacleBottom(spawn[0]);
			}
			else 
			{
				if (GameData.GAME_MODE_EVIL) //array.concat(EVIL, GOOD)
				{
					spawn.push(smallPool.checkOut(smallGoodFrom, smallGoodTo));
				}
				else 
				{
					spawn.push(smallPool.checkOut(smallEvilFrom, smallEvilTo));
				}
				spawnObstacleTop(spawn[0]);
			}
		}
		
		private function spawnObstacleTop(spawn:Obstacle):void 
		{
			levelManager.addElement(spawn, spawnPoint, GameData.MARGIN_Y_TOP - PositionData.OBSTACLE_NEGATIVE_MARGIN, PositionData.R_NEGATIVE_ROTATION);
		}
		
		private function spawnObstacleBottom(spawn:Obstacle):void 
		{
			levelManager.addElement(spawn, spawnPoint, GameData.MARGIN_Y_BOTTOM + PositionData.OBSTACLE_NEGATIVE_MARGIN, PositionData.R_NO_ROTATION);
		}
		
		private function spawnCoins(spawn:Vector.<Obstacle>, prevRandom:int):void 
		{
			var currentRandom:int = Math.random() * 10;
			if (prevRandom < 10) 
			{
				if (currentRandom < 3)  
				{
					//none
				}
				else if (currentRandom < 6) 
				{
					spawnProfit(spawn);
				}
				else if (currentRandom < 8) 
				{
					spawnJump(spawn);
				}
				else 
				{
					spawnGravity(spawn);
				}
			}
			else if (prevRandom < 20) 
			{	
				if (currentRandom < 3)  
				{
					//none
				}
				else 
				{
					spawnJump(spawn);
				}
			}
			else if (prevRandom < 35) 
			{					
				if (bPathIsUp) 
				{
					if (currentRandom < 3)  
					{
						//none
					}
					else if (currentRandom < 7) 
					{
						spawnProfit(spawn, OPTION_FORCE_TOP);
					}
					else
					{
						spawnJump(spawn, OPTION_FORCE_TOP);
					}
				}
				else 
				{
					if (currentRandom < 3)  
					{
						//none
					}
					else 
					{
						spawnGravity(spawn, OPTION_FORCE_BOTTOM);
					}
				}
			}
			else if (prevRandom < 50) 
			{
				if (bPathIsUp) 
				{
					if (currentRandom < 3)  
					{
						//none
					}
					else 
					{
						spawnGravity(spawn, OPTION_FORCE_TOP);
					}
				}
				else 
				{
					if (currentRandom < 3)  
					{
						//none
					}
					else if (currentRandom < 7) 
					{
						spawnProfit(spawn, OPTION_FORCE_BOTTOM);
					}
					else
					{
						spawnJump(spawn, OPTION_FORCE_BOTTOM);
					}
				}
			}
			else if (prevRandom < 75) 
			{
				if (bPathIsUp) 
				{
					if (currentRandom < 3)  
					{
						//none
					}
					else if (currentRandom < 7) 
					{
						spawnProfit(spawn, OPTION_FORCE_TOP);
					}
					else 
					{
						spawnJump(spawn, OPTION_FORCE_TOP);
					}
				}
				else 
				{
					if (currentRandom < 3)  
					{
						//none
					}
					else
					{
						spawnJump(spawn, OPTION_FORCE_BOTTOM);
					}
				}
			}
			else 
			{
				if (bPathIsUp) 
				{
					if (currentRandom < 3)  
					{
						//none
					}
					else
					{
						spawnJump(spawn, OPTION_FORCE_TOP);
					}
				}
				else 
				{
					if (currentRandom < 3)  
					{
						//none
					}
					else if (currentRandom < 7) 
					{
						spawnProfit(spawn, OPTION_FORCE_BOTTOM);
					}
					else 
					{
						spawnJump(spawn, OPTION_FORCE_BOTTOM);
					}
				}
			}
		}
		
		private function spawnCoinTop(coin:Obstacle, diff:int):void 
		{
			levelManager.addElement(coin, coinPoint + diff, GameData.MARGIN_Y_TOP + 50, PositionData.R_NEGATIVE_ROTATION);	
		}
		
		private function spawnCoinBottom(coin:Obstacle, diff:int):void 
		{
			levelManager.addElement(coin, coinPoint + diff, GameData.MARGIN_Y_BOTTOM - 50, PositionData.R_NO_ROTATION);
		}
		
		private function spawnProfit(spawn:Vector.<Obstacle>, force:int = OPTION_NO_FORCE):void 
		{
			var miracle:int = Math.random() * 100;
			var coin:Obstacle;
			for (var i:int = 0; i < coinNumb; i++) 
			{
				if (miracle == i) 
				{
					coin = powerPool.checkOut(1, 2);
				}
				else 
				{
					coin = coinPool.checkOut(0, 1);
				}
				if (force == OPTION_FORCE_TOP || force == OPTION_NO_FORCE && bPathIsUp) 
				{
					spawnCoinTop(coin, coinDiff * i);
				}
				else 
				{
					spawnCoinBottom(coin, coinDiff * i);
				}
				spawn.push(coin);
			}
		}
		
		private function spawnJump(spawn:Vector.<Obstacle>, force:int = OPTION_NO_FORCE):void 
		{
			var miracle:int = Math.random() * 100;
			var coin:Obstacle;
			for (var i:int = 1; i < coinNumb; i++) 
			{
				if (miracle == i) 
				{
					coin = powerPool.checkOut(1, 2);
				}
				else 
				{
					coin = coinPool.checkOut(0, 1);
				}
				var baseX:int = coinPoint + coinDiff * i;
				var superX:int = baseX - spawnPoint;
				var superY:int = ((superX * superX - PlayerData.PLAYER_JUMP_LENGTH_HELPER) / PlayerData.PLAYER_JUMP_LENGTH_HELPER) * PlayerData.PLAYER_JUMP_HEIGHT;
				var baseY:int;
				if (force == OPTION_FORCE_TOP || force == OPTION_NO_FORCE && bPathIsUp) 
				{
					baseY = GameData.MARGIN_Y_TOP;// + 10;
					levelManager.addElement(coin, baseX, baseY - superY, PositionData.R_NEGATIVE_ROTATION);	
				}
				else 
				{
					baseY = GameData.MARGIN_Y_BOTTOM;// - 10;
					levelManager.addElement(coin, baseX, baseY + superY, PositionData.R_NO_ROTATION);	
				}
				spawn.push(coin);
			}
		}
		
		private function spawnGravity(spawn:Vector.<Obstacle>, force:int = OPTION_NO_FORCE):void  
		{
			var miracle:int = Math.random() * 100;
			var coin:Obstacle;
			for (var i:int = 0; i < coinNumb; i++) 
			{
				if (miracle == i) 
				{
					coin = powerPool.checkOut(1, 2);
				}
				else 
				{
					coin = coinPool.checkOut(0, 1);
				}
				var baseX:int = spawnPoint - PlayerData.PLAYER_GRAVITY_CHANGE_LENGTH + (PlayerData.PLAYER_GRAVITY_CHANGE_LENGTH / coinNumb) * i;
				var superX:int = 0 + (PlayerData.PLAYER_GRAVITY_CHANGE_LENGTH / (coinNumb - 1)) * i;
				var superY:int = superX * PlayerData.PLAYER_GRAVITY_CHANGE_HEIGHT / PlayerData.PLAYER_GRAVITY_CHANGE_LENGTH;
				
				var baseY:int;
				if (force == OPTION_FORCE_TOP || force == OPTION_NO_FORCE && bPathIsUp) 
				{
					baseY = GameData.MARGIN_Y_TOP + 50;
					levelManager.addElement(coin, baseX, baseY + superY, PositionData.R_NEGATIVE_ROTATION);	
				}
				else 
				{
					baseY = GameData.MARGIN_Y_BOTTOM - 50;
					levelManager.addElement(coin, baseX, baseY - superY, PositionData.R_NO_ROTATION);	
				}
				spawn.push(coin);
			}
			
			togglePath();
		}
		
		private function createCollisions(spawn:Vector.<Obstacle>):void 
		{
			for (var i:int = 0; i < spawn.length; i++) 
			{
				hitManager.addElement(spawn[i]);
			}
		}
		
		private function togglePath():void 
		{
			if (bPathIsUp) 
			{
				bPathIsUp = false;
			}
			else 
			{
				bPathIsUp = true;
			}
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			atlas = null;
			levelManager = null;
			hitManager = null;
			smallPool.removeMe();
			smallPool = null;
			bigPool.removeMe();
			bigPool = null;
			coinPool.removeMe();
			coinPool = null;
		}
	}
}
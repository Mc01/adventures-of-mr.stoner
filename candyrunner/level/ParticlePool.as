package candyrunner.level 
{
	import candyrunner.core.ILayer;
	import starling.textures.Texture;
	
	public class ParticlePool implements ILayer
	{
		private var xml:XML;
		private var texture:Texture;
		private var capacity:int;
		private var pool:Vector.<ParticleEmitter>;
		
		public function ParticlePool(xml:XML, texture:Texture, capacity:int) 
		{
			this.xml = xml;
			this.texture = texture;
			this.capacity = capacity;
			init();
		}
		
		private function init():void 
		{
			pool = new Vector.<ParticleEmitter>();
			for (var i:int = 0; i < capacity; i++) 
			{
				var element:ParticleEmitter = new ParticleEmitter(xml, texture);
				pool.push(element);
			}
		}
		
		public function checkOut():ParticleEmitter 
		{
			if (capacity > 0) return pool[--capacity];
			else throw new Error("End of capacity.");
		}
		
		public function checkIn(element:ParticleEmitter):void 
		{
			pool[capacity++] = element;
		}
		
		public function removeMe():void 
		{
			for (var i:int = 0; i < pool.length; i++) 
			{
				pool[i].clear();
				pool[i].dispose();
				pool[i] = null;
			}
			xml = null;
			texture = null;
			pool = null;
		}
	}
}
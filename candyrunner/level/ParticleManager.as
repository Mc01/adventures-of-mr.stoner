package candyrunner.level 
{
	import candyrunner.core.ILayer;
	import candyrunner.data.PlayerData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class ParticleManager extends Sprite implements ILayer
	{
		[Embed(source = "../../../bin/assets/particle.pex", mimeType = "application/octet-stream")]
		private static const ParticleConfig:Class;
		
		[Embed(source = "../../../bin/assets/particle.png")]
		private static const ParticlePNG:Class;
		
		private var dispatcher:GameDispatcher;
		private var atlas:TextureAtlas;
		private var particlePool:ParticlePool;
		private var iter:int;
		private var iter2:int;
		private var aura:Sprite;
		private var ghost:Sprite;
		
		public function ParticleManager(atlas:TextureAtlas) 
		{
			this.atlas = atlas;
			dispatcher = GameDispatcher.instance;
			dispatcher.addEventListener(GameEvent.COIN_PARTICLES, onCoin);
			dispatcher.addEventListener(GameEvent.POWER_INIT, onPowerInit);
			dispatcher.addEventListener(GameEvent.POWER_FINISH, onPowerFinish);
			dispatcher.addEventListener(GameEvent.GHOST_READY, onGhostReady);
			dispatcher.addEventListener(GameEvent.GHOST_ON, onGhostOn);
			this.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
			init();
		}	
		
		private function onEnterFrame(e:Event):void 
		{
			iter ++;
			aura.x = PlayerData.POSITION_X - 10;
			aura.y = PlayerData.POSITION_Y - 5;
			aura.scaleX = Math.abs(Math.cos(2 * iter * Math.PI / 180));
			aura.scaleY = Math.abs(Math.cos(2 * iter * Math.PI / 180));
			if (aura.scaleX < 0.1)
				iter = 0;
				
			iter2++;
			
			
			ghost.x = PlayerData.POSITION_X - 70;
			ghost.y = PlayerData.POSITION_Y  + 14 *(Math.sin(10 * iter2 * Math.PI / 180));
		} 
		
		private function init():void 
		{
			particlePool = new ParticlePool(XML(new ParticleConfig), Texture.fromBitmap(new ParticlePNG), 40);
			initAura();
			initGhost();
		}
		
		private function initAura():void 
		{
			aura = new Sprite();
			aura.addChild(new Image(atlas.getTexture(TextureData.AURA)));
			aura.visible = false;
			aura.pivotX = aura.width / 2;
			aura.pivotY = aura.height / 2;
			addChild(aura);
		}
		
		private function initGhost():void
		{
			ghost = new Sprite();
			ghost.addChild(new Image(atlas.getTexture(TextureData.GHOST)));
			ghost.visible = false;
			ghost.pivotX = ghost.width / 2;
			ghost.pivotY = ghost.height / 2;
			
			addChild(ghost);
		}
		
		private function onCoin(e:GameEvent):void 
		{
			var data:Obstacle = Obstacle(e.data);
			var emitter:ParticleEmitter = particlePool.checkOut();
			emitter.addEventListener("complete", onComplete);
			Starling.juggler.add(emitter);
			emitter.emitterX = data.x - 45;
			if (data.rotation == 0) 
			{
				emitter.emitterY = data.y - 20;
			}
			else 
			{
				emitter.emitterY = data.y + 20;
			}
			addChild(emitter);
			emitter.start(0.1);
		}
		
		private function onComplete(e:Event):void 
		{
			var emitter:ParticleEmitter = e.target as ParticleEmitter;
			emitter.removeEventListeners();
			emitter.stop();
            Starling.juggler.remove(emitter);
            removeChild(emitter);
			particlePool.checkIn(emitter);
		}
		
		private function onPowerInit(e:GameEvent):void 
		{
			aura.visible = true;
		}
		
		private function onPowerFinish(e:GameEvent):void 
		{
			aura.visible = false;
		}
		
		private function onGhostReady(e:GameEvent):void 
		{
			iter2 = 0;
			ghost.visible = true;			
		}
		
		private function onGhostOn(e:GameEvent):void 
		{
			ghost.visible = false;
		}

		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			atlas = null;
			dispatcher.removeEventListener(GameEvent.COIN_PARTICLES, onCoin);
			dispatcher.removeEventListener(GameEvent.POWER_INIT, onPowerInit);
			dispatcher.removeEventListener(GameEvent.POWER_FINISH, onPowerFinish);
			dispatcher = null;
			removeChildren();
			particlePool.removeMe();
			particlePool = null;
			aura.dispose();
			aura = null;
			this.dispose();
		}
	}
}
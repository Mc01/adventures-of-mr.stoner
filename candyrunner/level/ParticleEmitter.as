package candyrunner.level 
{
	import starling.extensions.PDParticleSystem;
	import starling.textures.Texture;
	
	public class ParticleEmitter extends PDParticleSystem
	{
		private static const ONLY_PROFILER:String = "only_for_profiler";
		
		public function ParticleEmitter(config:XML, texture:Texture) 
		{
			super(config, texture);
		}	
	}
}
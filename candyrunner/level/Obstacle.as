package candyrunner.level 
{
	import candyrunner.core.IHitTestObject;
	import candyrunner.core.IUpdateableObject;
	import candyrunner.data.GameData;
	import candyrunner.data.PlayerData;
	import candyrunner.data.PositionData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import candyrunner.paralax.ParalaxObject;
	import flash.geom.Rectangle;
	import starling.display.Button;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class Obstacle extends ParalaxObject implements IHitTestObject
	{
		private var _pool:ObstaclePool;
		private var _type:String;
		private var dispatcher:GameDispatcher;
		
		public function Obstacle(type:String, textures:Vector.<Texture>, daddy:ObstaclePool) 
		{
			super(textures, GameData.OBSTACLE_DIVISOR);
			this._type = type;
			this._pool = daddy;
			dispatcher = GameDispatcher.instance;
		}
		
		override protected function updateEffect():void 
		{
			dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_HIT, this));
			dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_PARALAX, this));
			dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_POWER, this));
			dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_LEVEL, this));
		}
		
		public function hit(object:IHitTestObject):void 
		{
			if (_type == GameData.OBSTACLE_TYPE_HARMFUL && !PlayerData.GHOST_MODE_ACTIVE) 
			{		
				object.hit(this);
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_HIT, this));
			}
			else if (_type == GameData.OBSTACLE_TYPE_COIN && !PlayerData.GHOST_MODE_ACTIVE) 
			{
				GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.COIN_PARTICLES, this));
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, "coin");
				GameData.CAUGHT_COIN_NUMBER += 1;
				if (GameData.GHOST_COIN_NUMBER < GameData.GHOST_READY_NUMBER) 
				{
					if (GameData.GHOST_COIN_NUMBER == GameData.GHOST_READY_NUMBER - 1) 
					{
						GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.GHOST_READY));
					}
					GameData.GHOST_COIN_NUMBER += 1;
				}
				
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_HIT, this));
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_PARALAX, this));
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_POWER, this));
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_LEVEL, this));
			}
			else if (_type == GameData.OBSTACLE_TYPE_POWER && !PlayerData.GHOST_MODE_ACTIVE) 
			{
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_HIT, this));
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_PARALAX, this));
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CLEANER_LEVEL, this));
				dispatcher.dispatchEvent(new GameEvent(GameEvent.POWER_INIT));
			}
		}
		
		public function get hitRectangle():Rectangle 
		{
			if (this.rotation != 0) 
			{
				return new Rectangle(this.x - this.width/2, this.y, this.width, this.height);
			}
			else 
			{
				return new Rectangle(this.x - this.width/2, this.y - this.height, this.width, this.height);
			}
		}
		
		public function get pool():ObstaclePool 
		{
			return _pool;
		}
		
		public function get type():String 
		{
			return _type;
		}
		
		public override function removeMe():void 
		{
			dispatcher = null;
			_pool = null;
			super.removeMe();
		}
	}
}
package candyrunner.level 
{
	import candyrunner.assets.TextureCache;
	import candyrunner.core.GameObject;
	import candyrunner.core.ILayer;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class ObstaclePool implements ILayer
	{
		private var cache:TextureCache;
		private var pool:Vector.<Obstacle>;
		private var atlas:TextureAtlas;
		private var names:Array;
		private var capacity:int;
		private var type:String;
		
		public function ObstaclePool(atlas:TextureAtlas, names:Array, capacity:int, type:String)
		{
			this.atlas = atlas;
			this.names = names;
			this.capacity = capacity;
			this.type = type;
			init();
		}
		
		private function init():void 
		{
			cache = new TextureCache(atlas, names);
			pool = new Vector.<Obstacle>();
			for (var i:int = 0; i < capacity; i++) 
			{
				var element:Obstacle = new Obstacle(type, cache.textures, this);
				element.name = "obst" + i;
				if (element.type != GameData.OBSTACLE_TYPE_HARMFUL) 
				{
					element.pivotY = element.height / 2;
				}
				pool.push(element);
			}
		}
		
		public function checkOut(from:int, to:int):Obstacle //frames @from @to
		{
			if (capacity > 0) 
			{
				var object:Obstacle = pool.pop();
				object.currentFrame = from + int(Math.random() * (to - from));
				object.readjustSize();
				capacity--;
				return object;
			}
			else throw new Error("End of capacity.");
		}
		
		public function checkIn(object:Obstacle):void 
		{
			if (pool.indexOf(object) == -1) 
			{
				pool.push(object);
				capacity++;
				//trace("I GOT: ", object.name);
			}
		}
		
		public function removeMe():void 
		{
			
			for (var i:int = 0; i < pool.length; i++) 
			{
				pool[i].removeMe();
				pool[i] = null;
			}
			for (var j:int = 0; j < names.length; j++) 
			{
				names[i] = null;
			}		
			
			cache.removeMe();
			cache = null;
			pool = null;
			atlas = null;
			names = null;
		}
	}
}
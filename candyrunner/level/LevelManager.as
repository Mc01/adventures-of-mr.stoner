package candyrunner.level 
{
	import candyrunner.assets.TextureCache;
	import candyrunner.core.GameObject;
	import candyrunner.core.ILayer;
	import candyrunner.core.IUpdateableObject;
	import candyrunner.data.GameData;
	import candyrunner.data.PositionData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import candyrunner.paralax.ParalaxObject;
	import candyrunner.screens.InfoScreen;
	import flash.text.TextField;
	import starling.display.DisplayObject;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class LevelManager extends Sprite implements ILayer
	{
		private var atlas:TextureAtlas;
		private var _paralaxVector:Vector.<IUpdateableObject>;
		private var dispatcher:GameDispatcher;
		private var particleManager:ParticleManager;
		
		public function LevelManager(atlas:TextureAtlas)
		{
			super();
			this.atlas = atlas;
			init();
		}
		
		private function init():void 
		{
			_paralaxVector = new Vector.<IUpdateableObject>();
			createLevel();
			GameDispatcher.instance.addEventListener(GameEvent.CLEANER_LEVEL, onLevelHander);
		}
		
		private function createLevel():void 
		{
			var backgrounds:Vector.<ParalaxObject> = new Vector.<ParalaxObject>();
			var midgroundsEvil:Vector.<ParalaxObject> = new Vector.<ParalaxObject>();
			var midgroundsGood:Vector.<ParalaxObject> = new Vector.<ParalaxObject>();
			var foregroundsEvil:Vector.<ParalaxObject> = new Vector.<ParalaxObject>();
			var foregroundsGood:Vector.<ParalaxObject> = new Vector.<ParalaxObject>();
			
			var backgroundCache:TextureCache = new TextureCache(atlas, TextureData.BACKGROUNDS);
			var midEvilCache:TextureCache = new TextureCache(atlas, TextureData.MIDGROUNDS_EVIL);
			var midGoodCache:TextureCache = new TextureCache(atlas, TextureData.MIDGROUNDS_GOOD);
			var forEvilCache:TextureCache = new TextureCache(atlas, TextureData.FOREGROUNDS_EVIL);
			var forGoodCache:TextureCache = new TextureCache(atlas, TextureData.FOREGROUNDS_GOOD);
			
			if (GameData.GAME_MODE_EVIL) 
			{
				initElement(backgrounds, backgroundCache.textures, GameData.BACKGOUND_DIVISOR, GameData.SCREEN_HEIGHT / 2, PositionData.R_NEGATIVE_ROTATION);
				initElement(midgroundsEvil, midEvilCache.textures, GameData.MIDGROUND_DIVISOR, PositionData.Y_BOTTOM_SCREEN - PositionData.MIDGROUND_MARGIN, PositionData.R_NO_ROTATION);
				initElement(midgroundsGood, midGoodCache.textures, GameData.MIDGROUND_DIVISOR, PositionData.Y_TOP_SCREEN + PositionData.MIDGROUND_MARGIN, PositionData.R_NEGATIVE_ROTATION);
				initForeground(foregroundsEvil, forEvilCache.textures, GameData.FOREGROUND_DIVISOR, PositionData.Y_BOTTOM_SCREEN, PositionData.R_NO_ROTATION);
				initForeground(foregroundsGood, forGoodCache.textures, GameData.FOREGROUND_DIVISOR, PositionData.Y_TOP_SCREEN, PositionData.R_NEGATIVE_ROTATION);
			}
			else 
			{
				initElement(backgrounds, backgroundCache.textures, GameData.BACKGOUND_DIVISOR, GameData.SCREEN_HEIGHT / 2, PositionData.R_NO_ROTATION);
				initElement(midgroundsEvil, midEvilCache.textures, GameData.MIDGROUND_DIVISOR, PositionData.Y_TOP_SCREEN + PositionData.MIDGROUND_MARGIN, PositionData.R_NEGATIVE_ROTATION);
				initElement(midgroundsGood, midGoodCache.textures, GameData.MIDGROUND_DIVISOR, PositionData.Y_BOTTOM_SCREEN - PositionData.MIDGROUND_MARGIN, PositionData.R_NO_ROTATION);
				initForeground(foregroundsEvil, forEvilCache.textures, GameData.FOREGROUND_DIVISOR, PositionData.Y_TOP_SCREEN, PositionData.R_NEGATIVE_ROTATION);
				initForeground(foregroundsGood, forGoodCache.textures, GameData.FOREGROUND_DIVISOR, PositionData.Y_BOTTOM_SCREEN, PositionData.R_NO_ROTATION);
			}
			
			var infoscreen:InfoScreen = new InfoScreen();
			addElement(infoscreen, 200, 320, 0);
			
			particleManager = new ParticleManager(atlas);
			addChild(particleManager);
			
			for each (var element:ParalaxObject in backgrounds) 
			{	
				element.pivotY = element.height / 2;
			}
		}
		
		private function initElement(elements:Vector.<ParalaxObject>, textures:Vector.<Texture>, divisor:Number, y:int, rotation:Number):void 
		{
			elements[0] = new ParalaxObject(textures, divisor);
			elements[1] = new ParalaxObject(textures, divisor);
			addElement(elements[0], PositionData.X_FIRST_SCREEN, y, rotation);
			addElement(elements[1], elements[0].x + elements[0].width - 2, y, rotation);
		}
		
		private function initForeground(elements:Vector.<ParalaxObject>, textures:Vector.<Texture>, divisor:Number, y:int, rotation:Number):void 
		{
			elements[0] = new ParalaxObject(textures, divisor);
			elements[1] = new ParalaxObject(textures, divisor);
			elements[2] = new ParalaxObject(textures, divisor);
			addElement(elements[0], 0, y, rotation);
			addElement(elements[1], elements[0].x + elements[0].width - 2, y, rotation);
			addElement(elements[2], elements[1].x + elements[1].width - 2, y, rotation);
		}
		
		public function addElement(element:IUpdateableObject, x:int, y:int, rotation:Number):void 
		{	
			DisplayObject(element).x = x;
			DisplayObject(element).y = y;
			DisplayObject(element).rotation = rotation;
			addChild(DisplayObject(element));
			_paralaxVector.push(element);
		}
		
		private function removeElement(element:ParalaxObject):void 
		{
			element.removeMe();
			removeChild(element);
		}
		
		private function onLevelHander(e:GameEvent):void 
		{
			var element:Obstacle = Obstacle(e.data);
			removeChild(element);
			element.pool.checkIn(element);
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			GameDispatcher.instance.removeEventListener(GameEvent.CLEANER_LEVEL, onLevelHander);
			atlas = null;
			
			for (var i:int = 0; i < _paralaxVector.length; i++) 
			{
				if (_paralaxVector[i] != null) 
				{
					if (_paralaxVector[i] is InfoScreen)
					{
						InfoScreen(_paralaxVector[i]).removeMe();
						_paralaxVector[i] = null;
					}
					else
					{
						removeElement(ParalaxObject(_paralaxVector[i]));
						_paralaxVector[i] = null;
					}
				}
			}
			_paralaxVector = null;
			removeChildren();
			particleManager.removeMe();
			particleManager = null;
		}
		
		public function get paralaxVector():Vector.<IUpdateableObject> 
		{
			return _paralaxVector;
		}
	}
}
package candyrunner.core
{
	public interface IUpdateableObject
	{
		function update():void;
	}
}
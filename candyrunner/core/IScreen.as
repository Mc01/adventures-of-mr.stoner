package candyrunner.core 
{
	public interface IScreen 
	{
		function disable():void;
		function enable():void;
	}
}
package candyrunner.core
{
	import candyrunner.data.GameData;
	import candyrunner.level.Obstacle;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import candyrunner.player.Hero;
	import flash.geom.Rectangle;
	import starling.display.MovieClip;
	
	import starling.display.Quad;
	import starling.display.Sprite;

	public class HitTestManager implements ILayer
	{
		private var hitVector:Vector.<IHitTestObject>;
		private var quads:Vector.<Quad>;
		private var _debugSprite:Sprite;
		private var toRemove:Vector.<IHitTestObject>;
		private var hero:Hero;
		
		public function HitTestManager(hero:Hero)
		{
			init();
			this.hero = hero;
		}
		
		private function init():void 
		{
			hitVector = new Vector.<IHitTestObject>();
			quads = new Vector.<Quad>();
			_debugSprite = new Sprite();
			toRemove = new Vector.<IHitTestObject>();
			
			GameDispatcher.instance.addEventListener(GameEvent.CLEANER_HIT, onHitHandler);
		}
		
		private function onHitHandler(e:GameEvent):void 
		{
			var element:Obstacle = Obstacle(e.data);
 			toRemove.push(element);
		}
		
		public function checkCollision(element:IHitTestObject):void
		{
			for each(var hitElement:IHitTestObject in hitVector)
			{
				if(hitElement.hitRectangle.intersects(element.hitRectangle))
				{
					hitElement.hit(element);
				}
			}
			
			clean();
		}
		
		private function clean():void 
		{
			for (var i:int = 0; i < toRemove.length; i++) 
			{
				var index:int = hitVector.indexOf(toRemove[i]);
				if (index != -1) 
				{
					hitVector.splice(index, 1);
				}
			} 
			toRemove.splice(0, toRemove.length);
		}
		
		public function debugDraw():void
		{
			for each(var currentQuad:Quad in quads)
			{
				_debugSprite.removeChild(currentQuad);
			}
			
			var rHero:Rectangle = hero.hitRectangle;
			var qHero:Quad = new Quad(rHero.width, rHero.height, 0xFF11CC);
			qHero.x = rHero.x;
			qHero.y = rHero.y;
			qHero.alpha = 0.5;
			
			quads = new Vector.<Quad>();
			quads.push(qHero);
			_debugSprite.addChild(quads[0]);
			
			for each(var hitElement:IHitTestObject in hitVector)
			{
				var rect:Rectangle = hitElement.hitRectangle;
				var quad:Quad = new Quad(rect.width, rect.height, 0xFF77AA);
				quad.x = rect.x;
				quad.y = rect.y;
				quad.alpha = 0.5;
				_debugSprite.addChild(quad);
				quads.push(quad);
			}
		}
		
		public function addElement(element:IHitTestObject):void
		{
			hitVector.push(element);
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			GameDispatcher.instance.removeEventListener(GameEvent.CLEANER_HIT, onHitHandler);
			hero = null;			
			
			for each (var element:IHitTestObject in hitVector) 
			{
				element = null;
			}
			for each (var quad:Quad in quads) 
			{
				quad.dispose();
				quad = null;
			}
			for each (var remove:IHitTestObject in toRemove) 
			{
				remove = null;
			}
			
			_debugSprite.removeChildren();
			hitVector = null;
			quads = null;
			_debugSprite = null;
			toRemove = null;
		}
		
		public function get debugSprite():Sprite 
		{
			return _debugSprite;
		}
	}
}
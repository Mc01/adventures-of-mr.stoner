package candyrunner.core 
{
	public interface IView 
	{
		function init():void;
		function clean():void;
	}
}
package candyrunner.core
{
	import starling.display.MovieClip;
	import starling.textures.Texture;
	
	public class GameObject extends MovieClip
	{
		public function GameObject(textures:Vector.<Texture>)
		{
			super(textures, 1);
			this.pivotX = this.width / 2;
			this.pivotY = this.height;	
		}
	}
}
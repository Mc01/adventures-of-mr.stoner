package candyrunner.core 
{
	
	public interface IMultiplyable 
	{
		function multiplySpeed(multiplier:Number):void;
	}
	
}
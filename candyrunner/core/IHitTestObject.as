package candyrunner.core
{
	import flash.geom.Rectangle;

	public interface IHitTestObject
	{
		function hit(objet:IHitTestObject):void;
		function get hitRectangle():Rectangle;
	}
}
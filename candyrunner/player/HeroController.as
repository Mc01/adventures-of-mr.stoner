package candyrunner.player 
{
	import candyrunner.core.ILayer;
	import candyrunner.data.GameData;
	import candyrunner.data.PlayerData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.swiper.SwipeEvent;
	import candyrunner.swiper.SwipeParams;
	import candyrunner.swiper.Swiper;
	import flash.events.Event;
	import flash.events.TransformGestureEvent;
	import flash.geom.Rectangle;
	import starling.core.Starling;
	import starling.display.Stage;
	import starling.events.KeyboardEvent;
	/**
	 * ...
	 * @author 
	 */
	public class HeroController implements ILayer
	{
		
		private var stage:Stage;
		private var hero:Hero;
		private var _swiper:Swiper;
		
		public function HeroController(hero:Hero) 
		{
			stage = Starling.current.stage;
			this.hero = hero;
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			init();
		}
		
		private function init():void
		{
			_swiper = new Swiper(hero.parent, new Rectangle(0, 0, GameData.SCREEN_WIDTH, GameData.SCREEN_HEIGHT));
		   _swiper.addEventListener(SwipeEvent.SWIPE_ENDED, swipeEndedHandler);
		   GameDispatcher.instance.addEventListener("DISABLE_SWIPE", onDisableSwipe);
		}		
		
		private function onDisableSwipe(e:starling.events.Event):void 
		{
			if (_swiper != null) 
			{
				 _swiper.removeEventListener(SwipeEvent.SWIPE_ENDED, swipeEndedHandler);
			}
		}
		
		//SWIPER
		
		private function swipeEndedHandler(e:SwipeEvent):void 
		{
			trace("end");
			var params:SwipeParams = SwipeParams(e.data);
			
			if (!GameData.PAUSE_ACTIVE)
			{
				trace(params.pixelDistanceY);
				if (params.pixelDistanceY > 30 && hero.position == PlayerData.UP_POSITION)
				hero.changeGravity();
				else if (params.pixelDistanceY < - 30 && hero.position == PlayerData.DOWN_POSITION)
				hero.changeGravity();
				else if (Math.abs(params.pixelDistanceY) < 30)
				hero.jump();
			}
		}
		
		//-----------KEYBOARD_CONTROLS--------------
		
		
		public function onKeyDownHandler(event:KeyboardEvent):void
		{			 
			switch(event.keyCode)
			{
				
				case 71:
					{
						if (!PlayerData.GHOST_MODE_ACTIVE) 
						{
							GameData.GHOST_COIN_NUMBER = GameData.GHOST_READY_NUMBER;
							GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.GHOST_READY));
						}

						break;
					}
				
				case 32:
					{
						if(!GameData.PAUSE_ACTIVE)
							hero.jump();
						break;
					}
					
				case 40:
					{
						if (hero.position == PlayerData.UP_POSITION && !GameData.PAUSE_ACTIVE)
							hero.changeGravity();
						break;
					}
				
				case 38:
					{
						if (hero.position == PlayerData.DOWN_POSITION && !GameData.PAUSE_ACTIVE)
							hero.changeGravity();
						break;
					}
				
				case 72:
					{
						hero.kill();
						break;
					}
					
				case 80:
					{
						/*GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.PAUSE));
						break;*/
					}
			}
		}
		
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{			
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
			stage = null;
			hero = null;
			
			 GameDispatcher.instance.removeEventListener("DISABLE_SWIPE", onDisableSwipe);
		   
		   _swiper.removeEventListener(SwipeEvent.SWIPE_ENDED, swipeEndedHandler);
		   _swiper.clean();
		   _swiper = null;
		}
				
	}

}
package candyrunner.player 
{
	import candyrunner.data.GameData;
	import flash.net.SharedObject;
	
	public class HeroScore 
	{
		static private var sharedObject:SharedObject = SharedObject.getLocal("ourCandyRunner");
		static private var easy:Vector.<int> = new Vector.<int>();
		static private var medium:Vector.<int> = new Vector.<int>();
		static private var hard:Vector.<int> = new Vector.<int>();
		
		public static function getHighscores():Array
		{
			easy[0] = sharedObject.data.easy_first;
			easy[1] = sharedObject.data.easy_second;
			easy[2] = sharedObject.data.easy_third;
			
			medium[0] = sharedObject.data.medium_first;
			medium[1] = sharedObject.data.medium_second;
			medium[2] = sharedObject.data.medium_third;
			
			hard[0] = sharedObject.data.hard_first;
			hard[1] = sharedObject.data.hard_second;
			hard[2] = sharedObject.data.hard_third;
			
			var array:Array = new Array();
			array.push(easy, medium, hard);
			return array;
		}
		
		public static function saveHighscores():void 
		{
			getHighscores(); //Fetch highscore array
			
			if (GameData.INIT_MULTIPLIER == GameData.INIT_EASY_MULTIPLIER) 
			{
				easy = sortScores(easy);
			}
			else if (GameData.INIT_MULTIPLIER == GameData.INIT_MEDIUM_MULTIPLIER) 
			{
				medium = sortScores(medium);
			}
			else
			{
				hard = sortScores(hard);
			}
			
			setHighscores();
		}
		
		static private function sortScores(scores:Vector.<int>):Vector.<int> 
		{
			var currentScore:int = GameData.CURRENT_SCORE;
			var holdScore:int;
			
			for (var i:int = 0; i < scores.length; i++) 
			{
				if (currentScore > scores[i]) 
				{
					holdScore = scores[i];
					scores[i] = currentScore;
					currentScore = holdScore;
				}
			}
			
			return scores;
		}
		
		static private function setHighscores():void 
		{
			sharedObject.data.easy_first = easy[0];
			sharedObject.data.easy_second = easy[1];
			sharedObject.data.easy_third = easy[2];
			
			sharedObject.data.medium_first = medium[0];
			sharedObject.data.medium_second = medium[1];
			sharedObject.data.medium_third = medium[2];
			
			sharedObject.data.hard_first = hard[0];
			sharedObject.data.hard_second = hard[1];
			sharedObject.data.hard_third = hard[2];
			
			sharedObject.flush();
		}
	}
}
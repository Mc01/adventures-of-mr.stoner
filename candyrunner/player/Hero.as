package candyrunner.player 
{
	import candyrunner.assets.TextureCache;
	import candyrunner.core.GameObject;
	import candyrunner.core.IHitTestObject;
	import candyrunner.core.ILayer;
	import candyrunner.core.IUpdateableObject;
	import candyrunner.data.GameData;
	import candyrunner.data.PlayerData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import candyrunner.paralax.ParalaxManager;
	import flash.system.System;
	import starling.animation.Juggler;
	import starling.display.MovieClip;
	import starling.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class Hero extends GameObject implements IHitTestObject, IUpdateableObject, ILayer 
	{
			
		private var action:int;
		private var _position:int;
		private var dieDirection:int;
		
		private var _ghostModeTimer:Timer;
		private var ghostModeUsingTimer:Timer;
		
		private var isJumping:Boolean;
		private var isChangingGravity:Boolean;
		private var fakePositionX:int;
		private var fakepositionY:int;
		private var basePositionY:int;
		
		private var endGame:Boolean;
		
		private var tick:int;
		
		private var juggler:Juggler;
		
		private var iter:int = 0;
		
		
		public function Hero(texture:Vector.<Texture>) 
		{
			super(texture);
			addEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
		}
		
		private function onAddedHandler(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
			init();
		}
		
		public function init():void
		{
			this.pivotX = this.width / 2;
			this.pivotY = this.height / 2;
			
			PlayerData.INIT_POSITION_Y  = GameData.MARGIN_Y_TOP + this.height / 2;
			PlayerData.PLAYER_GRAVITY_CHANGE_HEIGHT = GameData.SCREEN_HEIGHT - 2 * PlayerData.INIT_POSITION_Y;
			PlayerData.PLAYER_DISTANCE = 0;
			GameData.CAUGHT_COIN_NUMBER = 0;
			GameData.GHOST_COIN_NUMBER = 0;
			
			this.x = PlayerData.INIT_POSITION_X;
			this.y = GameData.SCREEN_HEIGHT - PlayerData.INIT_POSITION_Y;
			
			PlayerData.GHOST_MODE_ACTIVE = false;
			isChangingGravity = false;
			isJumping = false;
			
			action = PlayerData.MOVE_NONE;
			_position = PlayerData.DOWN_POSITION;
			
			endGame = false;
			
			GameData.PAUSE_ACTIVE = false;
			
			juggler = new Juggler();
			juggler.add(this);
		}
				
		//-------------TIMER-----------------
		
		private function onTick(event:TimerEvent):void 
		{
			if(this.alpha == 1)
				this.alpha = 0.2;
			else
				this.alpha = 1;
			
			tick--;
			GameData.GHOST_COIN_NUMBER = Number(tick / 15) * GameData.GHOST_READY_NUMBER;
		}
		
		private function onTimerCompleteHandle(event:TimerEvent):void 
		{
			PlayerData.GHOST_MODE_ACTIVE = false;
			this.alpha = 1;
			
			_ghostModeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteHandle);
			_ghostModeTimer.removeEventListener(TimerEvent.TIMER, onTick);
			_ghostModeTimer = null;
			
			GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.GHOST_OFF));
			//trace("ghostMode ended");
		}
		
		//-------------ACTIONS----------------
		
		public function jump():void
		{
			if (!isJumping && !isChangingGravity && action != PlayerData.DIE)
			{
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, "jump");
				fakePositionX = -PlayerData.PLAYER_JUMP_HALF_LENGTH;
				isJumping = true;
				basePositionY = this.y;
				action = PlayerData.JUMP;
			}
		}
		
		public function changeGravity():void
		{
			if (!isJumping && !isChangingGravity && action != PlayerData.DIE)
			{
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, "gravity");
				fakePositionX = 0;
				isChangingGravity = true;
				basePositionY = this.y;
				action = PlayerData.CHANGE_GRAVITY;
			}	
		}
		
		public function hit(object:IHitTestObject):void 
		{
			if (!PlayerData.GHOST_MODE_ACTIVE && action != PlayerData.HIT && action != PlayerData.DIE)
			{
				kill();
			}
		}
		
		public function kill():void
		{
			if (!PlayerData.GHOST_MODE_ACTIVE && action != PlayerData.HIT && action != PlayerData.DIE)
				action = PlayerData.HIT;
		}
	
		
		//------------------IUpdateable interface methods
		
		public function update():void 
		{
			PlayerData.POSITION_X = this.x;
			PlayerData.POSITION_Y = this.y;
			
			PlayerData.PLAYER_DISTANCE += GameData.OVERALL_SPEED;
			
			switch(this.action)
			{
				case PlayerData.JUMP:
				{
					this.currentFrame = 24;
					fakePositionX += GameData.OVERALL_SPEED;
					this.y = basePositionY + _position * PlayerData.PLAYER_JUMP_HEIGHT * (fakePositionX*fakePositionX - PlayerData.PLAYER_JUMP_LENGTH_HELPER)/PlayerData.PLAYER_JUMP_LENGTH_HELPER;
					if(fakePositionX > PlayerData.PLAYER_JUMP_HALF_LENGTH)
					{
						this.action = PlayerData.MOVE_NONE;
						this.y = basePositionY;
						isJumping = false;
						
						currentFrame = 4;
					}
					break;
				}
					
				case PlayerData.CHANGE_GRAVITY:
				{
					currentFrame = 26;
					
					//Kwadratowka
					/*fakePositionX += GameData.OVERALL_SPEED;
					fakepositionY = (fakePositionX*fakePositionX)/(PlayerData.PLAYER_GRAVITY_CHANGE_LENGTH * PlayerData.PLAYER_GRAVITY_CHANGE_LENGTH) * PlayerData.PLAYER_GRAVITY_CHANGE_HEIGHT; 
					this.y = basePositionY - position * fakepositionY;*/
					
					fakePositionX += GameData.OVERALL_SPEED;
					fakepositionY = PlayerData.PLAYER_GRAVITY_CHANGE_HEIGHT/PlayerData.PLAYER_GRAVITY_CHANGE_LENGTH * fakePositionX; 
					this.y = basePositionY - _position * fakepositionY;
					
					if(fakePositionX > PlayerData.PLAYER_GRAVITY_CHANGE_LENGTH)
					{
						this.action = PlayerData.MOVE_NONE;
						this.y = basePositionY - _position * PlayerData.PLAYER_GRAVITY_CHANGE_HEIGHT;
						_position *= ( -1);
						this.scaleX *= ( -1);
						isChangingGravity = false;
					}
					break;
				}
				
				case PlayerData.HIT:
				{
						if (GameData.GHOST_COIN_NUMBER == GameData.GHOST_READY_NUMBER)
						{
							GameData.OVERALL_SPEED *= GameData.FAIL_MULTIPLIER;
							PlayerData.GHOST_MODE_ACTIVE = true;
							
							_ghostModeTimer = new Timer(PlayerData.GHOST_BLINK_DURATION, PlayerData.GHOST_BLINK_COUNT);
							_ghostModeTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteHandle);
							_ghostModeTimer.addEventListener(TimerEvent.TIMER, onTick);
							
							tick = 15;
							_ghostModeTimer.start();
							currentFrame = 25;
							action = PlayerData.FALLING;
							
							GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.GHOST_ON));
						}
						else
						{
							stage.removeEventListeners();
							GameData.OVERALL_SPEED *= GameData.END_GAME_MULTIPLIER;
							basePositionY = this.y;
							fakePositionX = -PlayerData.PLAYER_DIE_HALF_LENGTH;
							if (this.y > stage.stageHeight / 2)
								dieDirection = PlayerData.DOWN_POSITION;
							else
								dieDirection = PlayerData.UP_POSITION;
							action = PlayerData.DIE;
						}
					
					break;
				}
				
				case PlayerData.DIE:
				{
					currentFrame = 25;
					
					GameDispatcher.instance.dispatchEventWith("DISABLE_SWIPE");
					if (dieDirection == PlayerData.DOWN_POSITION)
					{
						fakePositionX += PlayerData.PLAYER_DIE_SPEED;
						this.y = basePositionY + PlayerData.PLAYER_DIE_HEIGHT * (fakePositionX*fakePositionX - PlayerData.PLAYER_DIE_LENGTH_HELPER)/PlayerData.PLAYER_DIE_LENGTH_HELPER;
						if (this.y > stage.stageHeight + this.height)
						{
							action = PlayerData.MOVE_NONE;
							GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.END_GAME));
						}
					}
					else
					{
						fakePositionX += PlayerData.PLAYER_DIE_SPEED;
						this.y = basePositionY - PlayerData.PLAYER_DIE_HEIGHT * (fakePositionX*fakePositionX - PlayerData.PLAYER_DIE_LENGTH_HELPER)/PlayerData.PLAYER_DIE_LENGTH_HELPER;
						if (this.y < -this.height)
						{
							action = PlayerData.MOVE_NONE;
							GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.END_GAME));
						}
					}
					break;
				}
				
				case PlayerData.FALLING:
				{
					currentFrame = 25;
					
					if (this.y > stage.stageHeight/2)
					{
						this.y += PlayerData.DYING_FALL_SPEED;
						if (this.y > stage.stageHeight - PlayerData.INIT_POSITION_Y)
						{
							this.y = stage.stageHeight - PlayerData.INIT_POSITION_Y;
							this._position = PlayerData.DOWN_POSITION;
							if (this.scaleX < 0)
								this.scaleX *= ( -1);
							action = PlayerData.MOVE_NONE;
							isChangingGravity = false;
							isJumping = false;
							break;
						}
					}
					else
					{
						this.y -= PlayerData.DYING_FALL_SPEED;
						if (this.y < PlayerData.INIT_POSITION_Y)
						{
							this.y = PlayerData.INIT_POSITION_Y;
							this._position = PlayerData.UP_POSITION;
							if (this.scaleX > 0)
								this.scaleX *= ( -1);
							action = PlayerData.MOVE_NONE;
							isChangingGravity = false;
							isJumping = false;
							break;
						}
					}
					break;
				}
					
				case PlayerData.MOVE_NONE:
				{					
					juggler.advanceTime(1);
					if (currentFrame == 24)
						currentFrame = 0;
					
					if(_position == PlayerData.DOWN_POSITION)
						this.rotation = 0;
					if(_position == PlayerData.UP_POSITION)
						this.rotation = 180 * (Math.PI/180);
				}
			}
			
			if(this.x < this.width/2)
				x=this.width/2;
			if(this.x + this.width/2 > stage.stageWidth)
				this.x = stage.stageWidth - this.width / 2;
				
			/*if (isChangingGravity)
			{
				if(position == PlayerData.DOWN_POSITION)
					this.rotation = 2 * (Math.PI) * (this.y - PlayerData.INIT_POSITION_Y) / PlayerData.PLAYER_GRAVITY_CHANGE_HEIGHT + (Math.PI);
				else
					this.rotation = 2 * (Math.PI) * (this.y - PlayerData.INIT_POSITION_Y) / PlayerData.PLAYER_GRAVITY_CHANGE_HEIGHT;
			}*/
			if (isChangingGravity)
				this.rotation = (Math.PI) * (this.y - PlayerData.INIT_POSITION_Y) / PlayerData.PLAYER_GRAVITY_CHANGE_HEIGHT + (Math.PI);
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{	
			if (_ghostModeTimer != null)
			{
				_ghostModeTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteHandle);
				_ghostModeTimer.removeEventListener(TimerEvent.TIMER, onTick);
				_ghostModeTimer = null;
			}
			
			this.removeEventListeners();
			this.dispose();
		}
		
		/* INTERFACE candyrunner.core.IHitTestObject */
		
		public function get hitRectangle():Rectangle 
		{
			return new Rectangle(this.x - this.width/2 + this.width/4, this.y - this.height/2, this.width/3, this.height);
		}
		
		/* GETTERS and SETTERS */
		
		public function get ghostModeTimer():Timer 
		{
			return _ghostModeTimer;
		}
		
		public function get position():int 
		{
			return _position;
		}
	}

}
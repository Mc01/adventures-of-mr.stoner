package candyrunner.paralax 
{
	import candyrunner.core.ILayer;
	import candyrunner.core.IUpdateableObject;
	import candyrunner.data.GameData;
	import candyrunner.data.PlayerData;
	import candyrunner.level.Obstacle;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.player.Hero;
	
	public class PowerManager implements IUpdateableObject, ILayer
	{
		private var hero:Hero;
		private var powerVector:Vector.<Obstacle>;
		private var toRemove:Vector.<Obstacle>;
		
		public function PowerManager(hero:Hero) 
		{
			this.hero = hero;
			powerVector = new Vector.<Obstacle>();
			toRemove = new Vector.<Obstacle>();
			GameDispatcher.instance.addEventListener(GameEvent.CLEANER_POWER, onPowerCleanerHandler);
			GameDispatcher.instance.addEventListener(GameEvent.POWER_COURIER, onPowerCourierHandler);
		}
		
		private function onPowerCleanerHandler(e:GameEvent):void 
		{
			var element:Obstacle = Obstacle(e.data);
			toRemove.push(element);
		}
		
		private function onPowerCourierHandler(e:GameEvent):void 
		{
			var dataVector:Vector.<Obstacle> = Vector.<Obstacle>(e.data);
			for each (var element:Obstacle in dataVector) 
			{
				powerVector.push(element);
			}
		}
		
		/* INTERFACE candyrunner.core.IUpdateableObject */
		
		public function update():void 
		{
			for each (var element:Obstacle in powerVector) 
			{
				element.x -= (element.x - hero.x) * GameData.OVERALL_SPEED / (8 * GameData.INIT_SPEED);
				element.y -= (element.y - hero.y) * GameData.OVERALL_SPEED / (8 * GameData.INIT_SPEED);
				if (PlayerData.GHOST_MODE_ACTIVE) 
				{
					GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.CLEANER_HIT, element));
					GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.CLEANER_POWER, element));
					GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.CLEANER_LEVEL, element));
				}
			}
			
			clean();
		}
		
		private function clean():void
		{
			for (var i:int = 0; i < toRemove.length; i++)
			{
				var index:int = powerVector.indexOf(toRemove[i]);
				if (index != -1)
				{
					powerVector.splice(index, 1);
				}
			}
			
			toRemove.splice(0, toRemove.length);
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			GameDispatcher.instance.removeEventListener(GameEvent.POWER_COURIER, onPowerCourierHandler);
			GameDispatcher.instance.removeEventListener(GameEvent.CLEANER_POWER, onPowerCleanerHandler);
			
			hero = null;
			
			for (var i:int = 0; i < toRemove.length; i++) 
			{
				toRemove[i] = null;
			}
			toRemove = null;
			
			for each (var element:Obstacle in powerVector) 
			{
				element.removeMe();
				element = null;
			}
			powerVector = null;
		}
	}
}
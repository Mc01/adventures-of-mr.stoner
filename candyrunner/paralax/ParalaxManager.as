package candyrunner.paralax
{
	import candyrunner.core.ILayer;
	import candyrunner.core.IUpdateableObject;
	import candyrunner.data.GameData;
	import candyrunner.data.PlayerData;
	import candyrunner.level.Obstacle;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.TextureAtlas;
	
	public class ParalaxManager implements IUpdateableObject, ILayer
	{
		private var paralaxVector:Vector.<IUpdateableObject>;
		private var speedMultiplierTimer:Timer;
		private var toRemove:Vector.<IUpdateableObject>;
		private var bCollector:Boolean;
		private var powerTimer:Timer;
		
		public function ParalaxManager(paralaxVector:Vector.<IUpdateableObject>)
		{
			this.paralaxVector = paralaxVector;
			init();
		}
		
		private function init():void
		{
			bCollector = false;
			
			toRemove = new Vector.<IUpdateableObject>();
			GameData.OVERALL_SPEED = GameData.INIT_SPEED;
			GameData.SPEED_MULTIPLIER = GameData.INIT_MULTIPLIER;
			
			speedMultiplierTimer = new Timer(GameData.SPEED_MULTIPLIER_DELAY, 0);
			speedMultiplierTimer.addEventListener(TimerEvent.TIMER, onMultiply);
			startMultiply();
			
			GameDispatcher.instance.addEventListener(GameEvent.CLEANER_PARALAX, onParalaxCleanerHandler);
			GameDispatcher.instance.addEventListener(GameEvent.POWER_INIT, onPowerInitHandler);
		}
		
		private function onParalaxCleanerHandler(e:GameEvent):void 
		{
			var element:Obstacle = Obstacle(e.data);
			toRemove.push(element);
		}
		
		private function onPowerInitHandler(e:GameEvent):void 
		{
			if (powerTimer != null) 
			{
				powerTimer.stop();
				powerTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onPowerTimer);
			}
			powerTimer = new Timer(3000, 1);
			powerTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onPowerTimer);
			powerTimer.start();
			bCollector = true;
		}
		
		private function onPowerTimer(e:TimerEvent):void 
		{
			powerTimer.stop();
			powerTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onPowerTimer);
			powerTimer = null;
			GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.POWER_FINISH));
			bCollector = false;
		}
		
		public function startMultiply():void 
		{
			speedMultiplierTimer.start();
		}
		
		public function pauseMultiply():void 
		{
			speedMultiplierTimer.stop();
		}
		
		private function onMultiply(event:TimerEvent):void
		{
			GameData.OVERALL_SPEED *= GameData.SPEED_MULTIPLIER;
		}
		
		/* INTERFACE candyrunner.core.IUpdateableObject */
		
		public function update():void
		{
			collect();
			loop();
			clean();
		}
		
		private function collect():void 
		{
			if (bCollector && !PlayerData.GHOST_MODE_ACTIVE) 
			{
				var powerVector:Vector.<Obstacle> = new Vector.<Obstacle>();
				var lastParalax:int = paralaxVector.length - 1;
				for (var i:int = lastParalax; i > 0; i--) 
				{
					if (paralaxVector[i] is Obstacle) 
					{
						var element:Obstacle = Obstacle(paralaxVector[i]);
						if (element.type == GameData.OBSTACLE_TYPE_COIN && Math.abs(element.x - PlayerData.INIT_POSITION_X) < 100) 
						{
							powerVector.push(Obstacle(paralaxVector[i]));
							toRemove.push(paralaxVector[i]);
						}
					}
				}
				GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.POWER_COURIER, powerVector));
			}
		}
		
		private function loop():void 
		{
			for each (var element:IUpdateableObject in paralaxVector)
			{
				if (element != null) 
				{
					element.update();
				}
			}
		}
		
		private function clean():void
		{
			for (var i:int = 0; i < toRemove.length; i++)
			{
				var index:int = paralaxVector.indexOf(toRemove[i]);
				if (index != -1)
				{
					paralaxVector.splice(index, 1);
				}
			}
			toRemove.splice(0, toRemove.length);
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			if (powerTimer != null) 
			{
				powerTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onPowerTimer);
			}
			powerTimer = null;
			
			GameDispatcher.instance.removeEventListener(GameEvent.POWER_INIT, onPowerInitHandler);
			GameDispatcher.instance.removeEventListener(GameEvent.CLEANER_PARALAX, onParalaxCleanerHandler);
			
			pauseMultiply();
			speedMultiplierTimer.removeEventListener(TimerEvent.TIMER, onMultiply);
			
			for (var i:int = 0; i < toRemove.length; i++) 
			{
				ParalaxObject(toRemove[i]).removeMe();
				toRemove[i] = null;
			}
			
			toRemove = null;
			paralaxVector = null;
			speedMultiplierTimer = null;
		}
	}
}
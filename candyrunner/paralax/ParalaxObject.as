package candyrunner.paralax 
{
	import candyrunner.core.GameObject;
	import candyrunner.core.ILayer;
	import candyrunner.core.IUpdateableObject;
	import candyrunner.data.GameData;
	import starling.textures.Texture;
	
	public class ParalaxObject extends GameObject implements IUpdateableObject, ILayer
	{	
		private var divisor:Number;
		
		public function ParalaxObject(textures:Vector.<Texture>, divisor:Number) 
		{
			super(textures);
			this.divisor = divisor;	
			this.currentFrame = Math.random() * numFrames;
		}
		
		public function update():void 
		{
			this.x -= GameData.OVERALL_SPEED * divisor;
			if (this.x + this.width/2  <= 0)
			{				
				updateEffect();
			}
		}
		
		protected function updateEffect():void 
		{
			if (this.divisor == 1)
				this.x += (this.width - 2) * 3;
			else
				this.x += (this.width - 2) * 2;
			this.currentFrame = Math.random() * numFrames;
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			this.removeEventListeners();
			this.dispose();
		}
	}
}
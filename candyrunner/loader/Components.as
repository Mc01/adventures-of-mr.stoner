package candyrunner.loader 
{
	import candyrunner.assets.AssetsPaths;
	import candyrunner.views.ViewConfig;
	import flash.utils.Dictionary;
	
	public class Components 
	{
		public static const COMPONENT_DICTIONARY:Dictionary = new Dictionary();
							COMPONENT_DICTIONARY[ViewConfig.GAME_VIEW] = [AssetsPaths.GAME_PNG, AssetsPaths.GAME_XML];
							COMPONENT_DICTIONARY[ViewConfig.MENU_VIEW] = [AssetsPaths.MENU_PNG, AssetsPaths.MENU_XML]; 
							COMPONENT_DICTIONARY[ViewConfig.FONTS] = [AssetsPaths.FONT_PNG, AssetsPaths.FONT_FNT];
	}
}
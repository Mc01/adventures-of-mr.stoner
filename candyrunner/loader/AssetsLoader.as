package candyrunner.loader 
{
	import candyrunner.data.GameData;
	import candyrunner.main.GameDispatcher;
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class AssetsLoader 
	{
		private var _dispatcher:GameDispatcher;
		private var eventCompleteName:String;
		private var loaded:Boolean;
		private var loaderBuff:int = 0;
		
		private var _view:String;
		private var pathXML:String;
		private var pathPNG:String;
		private var bitmap:Bitmap;
		private var xml:XML;
		
		private var eventVector:Vector.<String>;
		private var loaderVector:Vector.<AssetsLoader>;
		
		public function AssetsLoader(view:String) 
		{
			_dispatcher = GameDispatcher.instance;
			_view = view;
			eventVector = new Vector.<String>();
			pathPNG = Components.COMPONENT_DICTIONARY[view][0];
			pathXML = Components.COMPONENT_DICTIONARY[view][1];
			loadAssets();
		}
		
		private function loadAssets():void
		{
			eventVector.push(pathPNG);
			eventVector.push(pathXML);
			loadAsset(pathPNG);
			loadDataXml(pathXML);
		}
		
		public function loadAsset(filePath:String):void
		{			
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgressStatus);
			loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, onLoaderReady);
			var fileRequest:URLRequest = new URLRequest(filePath);
			loader.load(fileRequest);
		}
		
		public function onProgressStatus(e:ProgressEvent):void
		{   
			GameData.BYTES_LOADED = e.bytesLoaded;
			GameData.BYTES_TO_LOAD = e.bytesTotal;
			_dispatcher.dispatchEventWith('UPDATE_BAR');
		}
		
		public function loadDataXml(filePath:String):void
		{
			var _xmlLoader:URLLoader = new URLLoader();
			_xmlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			_xmlLoader.addEventListener(Event.COMPLETE, onXmlLoader_COMPLETE);
			var fileRequest:URLRequest = new URLRequest(filePath);
			_xmlLoader.load(fileRequest);
		}
		
		private function onLoaderReady(e:flash.events.Event):void 
		{
			trace("Loading complete:", _view);
			GameData.FILES_LOADED++;
			if (GameData.FILES_LOADED == GameData.FILES_TO_LOAD)
				GameData.FILES_LOADED = GameData.FILES_TO_LOAD = 0;
			bitmap = Bitmap(LoaderInfo(e.currentTarget).content);			
			eventVector.splice(eventVector.indexOf(e.type), 1);
			if (eventVector.length == 0)
			{
				superFunction();
			}
		}
		
		private function onXmlLoader_COMPLETE(e:flash.events.Event):void 
		{
			xml = XML(e.target.data);			
			eventVector.splice(eventVector.indexOf(e.type), 1);
			if (eventVector.length == 0)
			{
				superFunction();
			}		
		}
		
		private function superFunction():void 
		{
			var array:Array = new Array()
			array.push(bitmap);
			array.push(xml);
			_dispatcher.dispatchEventWith(_view, false, array);
		}
		
		public function setEventCompleteName(name:String):void
		{
			eventCompleteName = name;
		}
		
		public function clean():void
		{
			_dispatcher = null;
			GameData.BYTES_LOADED = 0;
			GameData.BYTES_TO_LOAD = 0;
		}
	}
}

package candyrunner.screens 
{
	import candyrunner.core.IUpdateableObject;
	import candyrunner.data.GameData;
	import candyrunner.data.PlayerData;
	import candyrunner.paralax.ParalaxObject;
	import starling.display.Sprite;
	import starling.text.TextField;
	
	
	public class InfoScreen extends Sprite implements IUpdateableObject
	{
		private var textObject:TextField;
		private var meters:TextField;
		private var text:int
		
		public function InfoScreen() 
		{
			text = 0;
			textObject =  new TextField(400, 150, String(text), "Rustler", 100, 0xFFFFFF, false);
			meters =  new TextField(400, 150, "M", "Rustler", 50, 0xFFFFFF, false);
			meters.x = textObject.textBounds.width + textObject.x;
			meters.y += 10;
			
			addChild(textObject);
			addChild(meters);
		}
		
		public function update():void 
		{
			this.x -= GameData.OVERALL_SPEED;
			if (this.x + this.width  <= 0)
			{				
				updateEffect();
			}
		}
		
		protected function updateEffect():void 
		{
			this.x += 100 * GameData.ONE_METER_IN_PIXELS;
			this.text += 100;
			if (Number(text) / 1000 == int(text / 1000))
			{
				textObject.color = 0xff0000;
				meters.color = 0xff0000;
			}
			else
			{
				textObject.color = 0xffffff;
				meters.color = 0xffffff;
			}
			textObject.text = String(text);
			meters.x = textObject.textBounds.width + textObject.x - 25;
		}
		
		public function removeMe():void
		{
			this.removeChildren();
			textObject.dispose();
			textObject = null;
			meters.dispose();
			meters = null;
		}
		
	}

}
package candyrunner.screens 
{
	import candyrunner.assets.AssetsPaths;
	import candyrunner.loader.AssetsLoader;
	import candyrunner.loader.Components;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import candyrunner.main.SceneEvent;
	import candyrunner.main.SceneObject;
	import candyrunner.views.FadeView;
	import candyrunner.views.GameView;
	import candyrunner.views.HUDView;
	import candyrunner.views.MenuView;
	import candyrunner.views.PopupView;
	import candyrunner.views.PreloaderView;
	import candyrunner.views.ViewConfig;
	import flash.utils.Dictionary;
	import Preloader.GamePreloader;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class SceneManager
	{
		private var viewList:Dictionary;
		private var layerList:Dictionary;
		private var gameContainer:GameLayers;
		private var dispatcher:GameDispatcher;	
		
		public function SceneManager(gameLayer:GameLayers) 
		{
			dispatcher = GameDispatcher.instance;
			gameContainer = gameLayer;
			
			viewList = new Dictionary();
			layerList = new Dictionary();
			
			init();			
		}
		
		private function init():void
		{			
			viewList[ViewConfig.GAME_VIEW] = new GameView();
			viewList[ViewConfig.HUD_VIEW] = new HUDView();
			viewList[ViewConfig.FADE_VIEW] = new FadeView();
			viewList[ViewConfig.POPUP_VIEW] = new PopupView();
			viewList[ViewConfig.MENU_VIEW] = new MenuView();
			viewList[ViewConfig.PRELOADER_VIEW] = new PreloaderView();
			
			layerList[ViewConfig.GAME_VIEW] = new Sprite();
				gameContainer.addChild(layerList[ViewConfig.GAME_VIEW]);
			layerList[ViewConfig.HUD_VIEW] = new Sprite();
				gameContainer.addChild(layerList[ViewConfig.HUD_VIEW]);
			layerList[ViewConfig.FADE_VIEW] = new Sprite();
				gameContainer.addChild(layerList[ViewConfig.FADE_VIEW]);
			layerList[ViewConfig.POPUP_VIEW] = new Sprite();
				gameContainer.addChild(layerList[ViewConfig.POPUP_VIEW]);
			layerList[ViewConfig.MENU_VIEW] = new Sprite();
				gameContainer.addChild(layerList[ViewConfig.MENU_VIEW]);
				
			layerList[ViewConfig.PRELOADER_VIEW] = new Sprite();
				gameContainer.addChild(layerList[ViewConfig.PRELOADER_VIEW]);
				
			layerList[ViewConfig.PRELOADER_VIEW].addChild(viewList[ViewConfig.PRELOADER_VIEW]);
			viewList[ViewConfig.PRELOADER_VIEW].init();
			layerList[ViewConfig.PRELOADER_VIEW].removeChild(viewList[ViewConfig.PRELOADER_VIEW]);
			
			dispatcher.addEventListener(SceneEvent.CHANGE_SCENE, changeSceneHandler);
			dispatcher.addEventListener(GameEvent.REMOVE_PRELOADER, onRemovePreloader);
		}
		
		private function add(viewName:String):void
		{
			viewList[viewName].init();
			layerList[viewName].addChild(viewList[viewName]);
		}
		
		private function remove(viewName:String):void
		{
			layerList[viewName].removeChild(viewList[viewName]);
 			(viewList[viewName]).clean();
		}
		
		private function addPreloader():void
		{
			layerList[ViewConfig.PRELOADER_VIEW].addChild(viewList[ViewConfig.PRELOADER_VIEW]);
		}
		
		private function onRemovePreloader(e:GameEvent):void 
		{
			(viewList[ViewConfig.PRELOADER_VIEW]).minimizeBar();
			layerList[ViewConfig.PRELOADER_VIEW].removeChild(viewList[ViewConfig.PRELOADER_VIEW]);
		}
		
		private function changeSceneHandler(e:Event):void 
		{
			var sceneData:SceneObject = SceneObject(e.data);
			
			if (sceneData.usePreloader) 
			{
				addPreloader();
			}
			
			if (sceneData.unload != null) 
			{
				remove(sceneData.unload);
			}
			
			/*if (sceneData.unload == sceneData.load) 
			{
				dispatcher.dispatchEvent(new GameEvent(GameEvent.ONCE_AGAIN_ATLAS));
			}
			else*/ if (sceneData.usePreloader && sceneData.load != null) 
			{
				trace("Loading started:", sceneData.load);
				var assetLoader:AssetsLoader = new AssetsLoader(sceneData.load);
			}
			else if (sceneData.load != null) 
			{
				add(sceneData.load);
			}
		}
	}
}
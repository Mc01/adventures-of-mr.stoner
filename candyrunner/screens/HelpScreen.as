package candyrunner.screens 
{
	import candyrunner.core.ILayer;
	import candyrunner.core.IScreen;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.ui.ClassButton;
	import candyrunner.ui.InterfaceObject;
	import candyrunner.ui.MuteWrapper;
	import candyrunner.ui.TextObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureAtlas;
	
	public class HelpScreen extends Sprite implements IScreen, ILayer 
	{
		private var dispatcher:GameDispatcher;
		private var atlas:TextureAtlas;
		private var menuButton:ClassButton;
		private var muteWrapper:MuteWrapper;
		
		public function HelpScreen(atlas:TextureAtlas) 
		{
			dispatcher = GameDispatcher.instance;
			this.atlas = atlas;
			preInit();
		}
		
		private function preInit():void 
		{
			var menuScreen:Image = new Image(atlas.getTexture(TextureData.MENU_SCREEN));
			addChild(menuScreen);
			
			insertIconAndText(TextureData.HELP_CLICK, 95, 275, 150, 50, "TAP TO JUMP");
			insertIconAndText(TextureData.HELP_SWIPE, 520, 275, 150, 30, "SWIPE TO CHANGE GRAVITY");
			insertIconAndText(TextureData.HELP_COIN, 95, 450, 150, 30, "AVOID OBSTACLES    AND COLLECT COINS");
			insertIconAndText(TextureData.HELP_GHOST, 520, 450, 150, 30, "UNLOCK GHOST MODE THROUGH COINS");
			
			menuButton = new ClassButton(GameData.SCREEN_WIDTH * 0.73, GameData.SCREEN_HEIGHT * 0.9, "BACK", atlas);
			addChild(menuButton);
			
			muteWrapper = new MuteWrapper(atlas, GameData.SCREEN_WIDTH * 0.95, GameData.SCREEN_HEIGHT * 0.1);
			addChild(muteWrapper);
			
			menuButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
		}
		
		private function insertIconAndText(iconName:String, x:int, y:int, x_offset:int, y_offset:int, string:String):void 
		{
			var icon:InterfaceObject = new InterfaceObject(atlas.getTexture(iconName), false, x, y);
			addChild(icon);
			var textObject:TextObject = new TextObject(false, icon.x + x_offset, icon.y + y_offset, 300, 100, 36);
			textObject.text = string;
			addChild(textObject);
		}
		
		private function onTouchHandler(event:TouchEvent):void 
		{
			if (event.getTouch(menuButton, TouchPhase.BEGAN))
			{
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CHANGE_SCREEN, ScreenConfig.HELP_TO_MENU));
			}
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			dispatcher = null;
			atlas = null;
			removeChildren();
			muteWrapper.removeMe();
			menuButton.removeMe();
			muteWrapper = null;
			menuButton = null;
			this.dispose();
		}
		
		/* INTERFACE candyrunner.core.IScreen */
		
		public function disable():void 
		{
			muteWrapper.disable();
			menuButton.removeEventListeners();
			this.visible = false;
		}
		
		public function enable():void 
		{
			muteWrapper.enable();
			menuButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			this.visible = true;
		}
	}
}
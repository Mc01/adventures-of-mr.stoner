package candyrunner.screens 
{
	import candyrunner.core.ILayer;
	import candyrunner.core.IScreen;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.SceneEvent;
	import candyrunner.ui.ClassButton;
	import candyrunner.ui.InterfaceObject;
	import candyrunner.ui.MuteWrapper;
	import candyrunner.views.ViewConfig;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureAtlas;
	
	public class OptionScreen extends Sprite implements IScreen, ILayer 
	{
		private var atlas:TextureAtlas;
		private var easyButton:ClassButton;
		private var mediumButton:ClassButton;
		private var hardButton:ClassButton;
		private var menuButton:ClassButton;
		private var dispatcher:GameDispatcher;
		private var muteWrapper:MuteWrapper;
		
		public function OptionScreen(atlas:TextureAtlas) 
		{
			this.atlas = atlas;
			dispatcher = GameDispatcher.instance;
			preInit();
		}
		
		private function preInit():void 
		{
			var menuScreen:Image = new Image(atlas.getTexture(TextureData.MENU_SCREEN));
			addChild(menuScreen);
			
			easyButton = new ClassButton(GameData.SCREEN_WIDTH * 0.15, GameData.SCREEN_HEIGHT * 0.9, "NORMAL", atlas);
			addChild(easyButton);
			
			mediumButton = new ClassButton(GameData.SCREEN_WIDTH * 0.38, easyButton.y, "HARD", atlas);
			addChild(mediumButton);
			
			hardButton = new ClassButton(GameData.SCREEN_WIDTH * 0.61, mediumButton.y, "INSANE", atlas);
			addChild(hardButton);
			
			menuButton = new ClassButton(GameData.SCREEN_WIDTH * 0.84, hardButton.y, "BACK", atlas);
			addChild(menuButton);
			
			muteWrapper = new MuteWrapper(atlas, GameData.SCREEN_WIDTH * 0.95, GameData.SCREEN_HEIGHT * 0.1);
			addChild(muteWrapper);
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			dispatcher = null;
			atlas = null;
			removeChildren();
			muteWrapper.removeMe();
			easyButton.removeMe();
			mediumButton.removeMe();
			hardButton.removeMe();
			menuButton.removeMe();
			muteWrapper = null;
			easyButton = null;
			mediumButton = null;
			hardButton = null;
			menuButton = null;
			this.dispose();
		}
		
		/* INTERFACE candyrunner.core.IScreen */
		
		public function disable():void 
		{
			muteWrapper.disable();
			easyButton.removeEventListeners();
			mediumButton.removeEventListeners();
			hardButton.removeEventListeners();
			menuButton.removeEventListeners();
			this.visible = false;
		}
		
		public function enable():void 
		{
			muteWrapper.enable();
			this.visible = true;
			easyButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			mediumButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			hardButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			menuButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
		}
		
		private function onTouchHandler(e:TouchEvent):void 
		{
			
			if (e.getTouch(easyButton, TouchPhase.BEGAN))
			{
				trace("easy");
				GameData.INIT_SPEED = GameData.INIT_EASY_SPEED;
				GameData.INIT_MULTIPLIER = GameData.INIT_EASY_MULTIPLIER;
				GameData.FILES_TO_LOAD = 1;
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, GameData.CLICK);
				dispatcher.dispatchEvent(new SceneEvent(ViewConfig.MENU_VIEW, ViewConfig.GAME_VIEW, true));				
				
			
			}
			else if (e.getTouch(mediumButton, TouchPhase.BEGAN))
			{
				trace("medium");
				GameData.INIT_SPEED = GameData.INIT_MEDIUM_SPEED;
				GameData.INIT_MULTIPLIER = GameData.INIT_MEDIUM_MULTIPLIER;
				GameData.FILES_TO_LOAD = 1;
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, GameData.CLICK);
				dispatcher.dispatchEvent(new SceneEvent(ViewConfig.MENU_VIEW, ViewConfig.GAME_VIEW, true));
			}
			else if (e.getTouch(hardButton, TouchPhase.BEGAN))
			{
				trace("hard");
				GameData.INIT_SPEED = GameData.INIT_HARD_SPEED;
				GameData.INIT_MULTIPLIER = GameData.INIT_HARD_MULTIPLIER;
				GameData.FILES_TO_LOAD = 1;
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, GameData.CLICK);
				dispatcher.dispatchEvent(new SceneEvent(ViewConfig.MENU_VIEW, ViewConfig.GAME_VIEW, true));
			}
			else if (e.getTouch(menuButton, TouchPhase.BEGAN))
			{
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CHANGE_SCREEN, ScreenConfig.OPTION_TO_MENU));
			}
		}
	}
}
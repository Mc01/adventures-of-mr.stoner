package candyrunner.screens 
{
	import candyrunner.core.GameObject;
	import candyrunner.core.ILayer;
	import candyrunner.core.IScreen;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.GameManager;
	import candyrunner.main.SceneEvent;
	import candyrunner.ui.ClassButton;
	import candyrunner.ui.InterfaceObject;
	import candyrunner.ui.MuteWrapper;
	import candyrunner.views.ViewConfig;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class MenuScreen extends Sprite implements IScreen, ILayer
	{
		private var atlas:TextureAtlas
		private var dispatcher:GameDispatcher;
		private var startButton:ClassButton;
		private var helpButton:ClassButton;
		private var scoreButton:ClassButton;
		private var muteWrapper:MuteWrapper;
		
		public function MenuScreen(atlas:TextureAtlas) 
		{
			dispatcher = GameDispatcher.instance;
			this.atlas = atlas;
			preInit();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
		}
		
		private function preInit():void
		{
			var menuScreen:Image = new Image(atlas.getTexture(TextureData.MENU_SCREEN));
			addChild(menuScreen);
			
			startButton = new ClassButton(GameData.SCREEN_WIDTH * 0.27, GameData.SCREEN_HEIGHT * 0.9, "START", atlas);
			addChild(startButton);
			
			scoreButton = new ClassButton(GameData.SCREEN_WIDTH * 0.50, startButton.y, "SCORES", atlas);
			addChild(scoreButton);
			
			helpButton = new ClassButton(GameData.SCREEN_WIDTH * 0.73, startButton.y, "HELP", atlas);
			addChild(helpButton);
			
			muteWrapper = new MuteWrapper(atlas, GameData.SCREEN_WIDTH * 0.95, GameData.SCREEN_HEIGHT * 0.1);
			addChild(muteWrapper);
		}
		
		private function onAddedHandler(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
			enable();
		}
		
		private function onTouchHandler(event:TouchEvent):void 
		{
			if (event.getTouch(startButton, TouchPhase.BEGAN))
			{
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CHANGE_SCREEN, ScreenConfig.MENU_TO_OPTION));
			}
			else if (event.getTouch(scoreButton, TouchPhase.BEGAN)) 
			{
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CHANGE_SCREEN, ScreenConfig.MENU_TO_SCORES));
			}
			else if (event.getTouch(helpButton, TouchPhase.BEGAN)) 
			{
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CHANGE_SCREEN, ScreenConfig.MENU_TO_HELP));
			}
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			dispatcher = null;
			atlas = null;
			removeChildren();
			muteWrapper.removeMe();
			helpButton.removeMe();
			scoreButton.removeMe();
			startButton.removeMe();
			muteWrapper = null;
			helpButton = null;
			scoreButton = null;
			startButton = null;
			this.dispose();
		}
		
		/* INTERFACE candyrunner.core.IScreen */
		
		public function disable():void 
		{
			muteWrapper.disable();
			helpButton.removeEventListeners();
			scoreButton.removeEventListeners();
			startButton.removeEventListeners();
			this.visible = false;
		}
		
		public function enable():void 
		{
			muteWrapper.enable();
			this.visible = true;
			startButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			scoreButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			helpButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
		}
	}
}
package candyrunner.screens 
{
	import candyrunner.core.ILayer;
	import candyrunner.core.IScreen;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.player.HeroScore;
	import candyrunner.ui.ClassButton;
	import candyrunner.ui.InterfaceObject;
	import candyrunner.ui.MuteWrapper;
	import candyrunner.ui.TextObject;
	import flash.net.SharedObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureAtlas;
	
	public class ScoreScreen extends Sprite implements IScreen, ILayer 
	{
		private var dispatcher:GameDispatcher;
		private var atlas:TextureAtlas;
		private var menuButton:ClassButton;
		private var highscores:Array;
		private var textVector:Vector.<TextObject>;
		private var muteWrapper:MuteWrapper;
		
		public function ScoreScreen(atlas:TextureAtlas) 
		{
			dispatcher = GameDispatcher.instance;
			this.atlas = atlas;
			preInit();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
		}
		
		private function preInit():void 
		{
			var menuScreen:Image = new Image(atlas.getTexture(TextureData.MENU_SCREEN));
			addChild(menuScreen);
			
			menuButton = new ClassButton(GameData.SCREEN_WIDTH * 0.73, GameData.SCREEN_HEIGHT * 0.9, "BACK", atlas);
			addChild(menuButton);
			
			var easyText:TextObject = new TextObject(false, 200, 325, 150, 50, 38, false);
			easyText.text = "NORMAL";
			addChild(easyText);
			
			var mediumText:TextObject = new TextObject(false, easyText.x + 200, easyText.y, 150, 50, 38, false);
			mediumText.text = "HARD";
			addChild(mediumText);
			
			var hardText:TextObject = new TextObject(false, mediumText.x + 200, mediumText.y, 150, 50, 38, false);
			hardText.text = "INSANE";
			addChild(hardText);
			
			muteWrapper = new MuteWrapper(atlas, GameData.SCREEN_WIDTH * 0.95, GameData.SCREEN_HEIGHT * 0.1);
			addChild(muteWrapper);
		}
		
		private function onAddedHandler(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
			init();
		}
		
		private function init():void
		{
			highscores = HeroScore.getHighscores();
			
			textVector = new Vector.<TextObject>();
			for (var i:int = 0; i < highscores.length; i++) 
			{
				for (var j:int = 0; j < highscores[i].length; j++) 
				{
					var textObject:TextObject = new TextObject(false, 150 + 200 * i, 425 + 50 * j, 200, 50, 38, false);
					textObject.text = highscores[i][j].toString();
					addChild(textObject);
					textVector.push(textObject);
				}
			}		
			
			menuButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
		}
		
		private function onTouchHandler(event:TouchEvent):void 
		{
			if (event.getTouch(menuButton, TouchPhase.BEGAN))
			{
				dispatcher.dispatchEvent(new GameEvent(GameEvent.CHANGE_SCREEN, ScreenConfig.SCORES_TO_MENU));
			}
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			dispatcher = null;
			atlas = null;
			removeChildren();
			for each (var element:TextObject in textVector) 
			{
				element.removeMe();
				element = null;
			}
			textVector = null;
			muteWrapper.removeMe();
			muteWrapper = null;
			menuButton.removeMe();
			menuButton = null;
			this.dispose();
		}
		
		/* INTERFACE candyrunner.core.IScreen */
		
		public function disable():void 
		{
			muteWrapper.disable();
			menuButton.removeEventListeners();
			this.visible = false;
		}
		
		public function enable():void 
		{
			muteWrapper.enable();
			menuButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			this.visible = true;
		}
	}
}
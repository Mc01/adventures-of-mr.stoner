package candyrunner.screens 
{
	public class ScreenConfig 
	{
		public static const MENU_TO_OPTION:String = "menu_to_option";
		public static const MENU_TO_SCORES:String = "menu_to_scores";
		public static const MENU_TO_HELP:String = "menu_to_help";
		public static const OPTION_TO_MENU:String = "option_to_menu";
		public static const SCORES_TO_MENU:String = "scores_to_menu";
		public static const HELP_TO_MENU:String = "help_to_menu";
	}
}
package candyrunner.data 
{
	public class TextureData
	{
		public static const BACKGROUNDS:Array = ["background"]; //normal - good : negative - evil
		public static const FOREGROUNDS_EVIL:Array = ["foreground_evil"];
		public static const FOREGROUNDS_GOOD:Array = ["foreground_good"];
		public static const MIDGROUNDS_EVIL:Array = ["pieklo", "cmentarz", "las"];
		public static const MIDGROUNDS_GOOD:Array = ["tecza", "pianki", "lodowiec"];
		public static const OBSTACLES_SMALL_EVIL:Array = ["skulls", "grob"];
		public static const OBSTACLES_SMALL_GOOD:Array = ["choco", "puding"];
		public static const OBSTACLES_BIG_EVIL:Array = ["statue", "candle"];
		public static const OBSTACLES_BIG_GOOD:Array = ["icecream", "unicorn"];
		public static const OBSTACLES_COINS:Array = ["coin", "supercoin"];
		public static const PLAYER:Array = ["animacja_0001", "animacja_0002", "animacja_0003", "animacja_0004", "animacja_0005", "animacja_0006", "animacja_0007", "animacja_0008", "animacja_0009", "animacja_0010", "animacja_0011", "animacja_0012", "animacja_0013", "animacja_0014", "animacja_0015", "animacja_0016", "animacja_0017", "animacja_0018", "animacja_0019", "animacja_0020", "animacja_0021", "animacja_0022", "animacja_0023", "animacja_0024", "animacja_0025", "animacja_0026", "animacja_0027"];	
		public static const AURA:String = "aura";
		public static const GHOST:String = "DUSZEK";
		
		public static const SCORE_SMALL:String = "NEW HIGHSCORE !!!";
		public static const HIGHSCORE_TAB:String = "highscore 2";
		public static const SCORE_TAB:String = "SCORE";
		public static const PROGRESS_BASE:String = "1";
		public static const PROGRESS_BAR:String = "2";
		public static const PROGRESS_BAR_GHOSTING:String = "5";
		public static const PROGRESS_OVER:String = "3";
		public static const PROGRESS_DUCH:String = "4";
		public static const PAUSE_STOP:String = "STOP";
		public static const PAUSE_PLAY:String = "PLAY";
		public static const COUNTDOWN:Array = ["pauza3", "pauza2", "pauza1", "pauza-go"]
		
		public static const MENU_SCREEN:String = "INSTRUKCJA TLO";
		public static const CLASS_BUTTON:String = "BUTTON";
		public static const POPUP_SCREEN:String = "POPUP";
		
		public static const HELP_CLICK:String = "KLICK";
		public static const HELP_SWIPE:String = "SMYR";
		public static const HELP_COIN:String = "COIN";
		public static const HELP_GHOST:String = "GM";
		
		public static const SOUND_VOLUME:Array = ["SOUND OFF", "SOUND ON"]
	}
}
package candyrunner.data 
{
	import candyrunner.data.GameData;
	
	public class PlayerData 
	{
		public static const PLAYER_MAX_SPEED:Number = 13;
		public static var PLAYER_DISTANCE:Number = 0;
		
		public static var POSITION_X:int;
		public static var POSITION_Y:int;
			
		public static const INIT_POSITION_X:int = 100;
		public static var INIT_POSITION_Y:int;		//Foreground pixel height;
		public static const DOWN_POSITION:int = 1;
		public static const UP_POSITION:int = -1;		
		
		public static const JUMP:int = 2;
		public static const CHANGE_GRAVITY:int = 3;
		public static const HIT:int = 4;
		public static const DIE:int = 5;
		public static const FALLING:int = 6;
		public static const MOVE_NONE:int = 0;		
		
		public static const GHOST_MODE_DURATION:Number = 3000;		//Duration in miliseconds
		public static const GHOST_BLINK_DURATION:Number = 200;
		public static const GHOST_BLINK_COUNT:int = GHOST_MODE_DURATION / GHOST_BLINK_DURATION;
		
		public static const PLAYER_JUMP_HEIGHT:int = 150;
		public static const PLAYER_JUMP_LENGTH:int = 450;
		public static const PLAYER_JUMP_HALF_LENGTH:int = PLAYER_JUMP_LENGTH/2;
		public static const PLAYER_JUMP_LENGTH_HELPER:int = PLAYER_JUMP_HALF_LENGTH * PLAYER_JUMP_HALF_LENGTH;
		
		public static var PLAYER_GRAVITY_CHANGE_HEIGHT:int;  
		public static const PLAYER_GRAVITY_CHANGE_LENGTH:int = 200;
		public static const PLAYER_GRAVITY_CHANGE_LENGTH_HELPER:int = PLAYER_GRAVITY_CHANGE_LENGTH * PLAYER_GRAVITY_CHANGE_LENGTH;
		
		public static const PLAYER_DIE_HEIGHT:int  = 90;
		public static const PLAYER_DIE_LENGTH:int = 250;
		public static const PLAYER_DIE_HALF_LENGTH:int = PLAYER_DIE_LENGTH/2;
		public static const PLAYER_DIE_LENGTH_HELPER:int = PLAYER_DIE_HALF_LENGTH * PLAYER_DIE_HALF_LENGTH;
		public static const PLAYER_DIE_SPEED:int = 5
				
		public static const DYING_FALL_SPEED:Number = 3;
		public static var GHOST_MODE_ACTIVE:Boolean;
	}
}
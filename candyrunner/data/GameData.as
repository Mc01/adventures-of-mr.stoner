package candyrunner.data 
{
	public class GameData 
	{	
		public static const ONE_METER_IN_PIXELS:int = 50;
		public static const COIN_POINT_NUMBER:int = 3;
		
		public static const SCREEN_WIDTH:int = 1024;
		public static const SCREEN_HEIGHT:int = 768;
		
		public static const MARGIN_Y_TOP:int = 75;		//Foreground pixel height;
		public static const MARGIN_Y_BOTTOM:int = SCREEN_HEIGHT - MARGIN_Y_TOP;
		
		public static const INIT_EASY_SPEED:int = 8;
		public static const INIT_MEDIUM_SPEED:int = 10;
		public static const INIT_HARD_SPEED:int = 12;
		
		public static var INIT_SPEED:Number = 1;
		public static var OVERALL_SPEED:Number = 1;
		
		public static const INIT_EASY_MULTIPLIER:Number = 1.005;
		public static const INIT_MEDIUM_MULTIPLIER:Number = 1.007;
		public static const INIT_HARD_MULTIPLIER:Number = 1.009;
		
		public static var INIT_MULTIPLIER:Number = 1;
		public static var SPEED_MULTIPLIER:Number = 1;

		public static const SPEED_MULTIPLIER_DIVISOR:Number = 0.99995;
		public static const MIN_SPEED_MULTIPLIER:Number = 1.001;
		
		public static const STATIC_SPEED_MULTIPLIER:int = 1;
		public static const FAIL_MULTIPLIER:Number = 0.9;
		public static const END_GAME_MULTIPLIER:int  = 0;
		public static const SPEED_MULTIPLIER_DELAY:int = 500;  // in miliseconds;
		
		public static const PLAYER_DIVISOR:Number = 1;
		public static const OBSTACLE_DIVISOR:Number = 1;
		public static const FOREGROUND_DIVISOR:Number = 1;
		public static const MIDGROUND_DIVISOR:Number = 0.25;
		public static const BACKGOUND_DIVISOR:Number = 0.25 * MIDGROUND_DIVISOR;
		
		public static const OBSTACLE_BETWEEN:int = 600;
		public static const OBSTACLE_COIN_NUMB:int = 9;
		public static const OBSTACLE_COIN_DIFF:int = 50;
		
		public static const OBSTACLE_TYPE_HARMFUL:String = "obstacle";
		public static const OBSTACLE_TYPE_COIN:String = "coin";
		public static const OBSTACLE_TYPE_POWER:String = "power";
		
		public static var GAME_MODE_EVIL:Boolean = true;
		
		public static const MAX_ZERO_NUMBER:int = 6;
		public static const GHOST_READY_NUMBER:int = 100;
		
		public static var CAUGHT_COIN_NUMBER:int = 0;
		public static var GHOST_COIN_NUMBER:int = 0;
		public static var CURRENT_SCORE:int = 0;
		
		public static var BYTES_TO_LOAD:int;
		public static var BYTES_LOADED:int;
		public static var FILES_TO_LOAD:int;
		public static var FILES_LOADED:int;
		
		public static var PAUSE_ACTIVE:Boolean = false;
		
		public static var RADIAN:int = 0;
		
		public static const GAME_BACKGROUND:String = "anything";
		public static const MENU_BACKGROUND:String = "boing";
		public static const COIN_COLLECT:String = "coin";
		public static const JUMP:String = "jump";
		public static const GRAVITY:String = "gravity";
		public static const CLICK:String = "click";
		public static const GHOST_FIRE:String = "ghost_fire";
		
		public static var SOUNDS_VOLUME:int = 1;
	}
}
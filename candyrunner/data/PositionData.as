package candyrunner.data 
{
	public class PositionData 
	{
		public static const X_FIRST_SCREEN:int = int(GameData.SCREEN_WIDTH / 2);
		public static const X_SECOND_SCREEN:int = int(GameData.SCREEN_WIDTH * 3 / 2);
		public static const Y_TOP_SCREEN:int = 0;
		public static const Y_BOTTOM_SCREEN:int = GameData.SCREEN_HEIGHT;
		public static const R_NO_ROTATION:Number = 0;
		public static const R_NEGATIVE_ROTATION:Number = Math.PI;
		
		public static const MIDGROUND_MARGIN:int = 50;
		public static const BACKGROUND_MARGIN:int = 69;
		public static const OBSTACLE_NEGATIVE_MARGIN:int = 28;
	}
}
package candyrunner.ui 
{
	import candyrunner.core.GameObject;
	import candyrunner.core.ILayer;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.SceneEvent;
	import candyrunner.views.ViewConfig;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class PauseButton extends Sprite implements ILayer 
	{
		private var atlas:TextureAtlas;
		private var pauseButton:InterfaceObject;
		private var bPause:Boolean = false;
		
		public function PauseButton(atlas:TextureAtlas, x:int, y:int) 
		{
			this.atlas = atlas;
			this.x = x;
			this.y = y;
			init();
		}
		
		private function init():void 
		{
			pauseButton = new InterfaceObject(atlas.getTexture(TextureData.PAUSE_STOP), false, 0, 0);
			addChild(pauseButton);
			pauseButton.addEventListener(TouchEvent.TOUCH, onTouchHandler);
			
			GameDispatcher.instance.addEventListener(GameEvent.END_GAME, onEndHandler);
			GameDispatcher.instance.addEventListener(GameEvent.PAUSE, onPause);
			GameDispatcher.instance.addEventListener(GameEvent.UNPAUSE, onUnpause);
		}
		
		private function onEndHandler(e:GameEvent):void 
		{
			if (pauseButton != null) 
			{
				pauseButton.removeEventListeners();
			}
			GameDispatcher.instance.dispatchEvent(new SceneEvent(ViewConfig.HUD_VIEW, ViewConfig.POPUP_VIEW));
			GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.DISABLE_UNPAUSE));
		}
		
		private function onTouchHandler(e:TouchEvent):void 
		{
			if (e.getTouch(pauseButton, TouchPhase.BEGAN) && !bPause)
			{
				e.stopPropagation();
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, GameData.CLICK);
				GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.PAUSE));
				GameDispatcher.instance.dispatchEvent(new SceneEvent(null, ViewConfig.POPUP_VIEW));
			}
		}
		
		private function onPause(e:GameEvent):void 
		{
			
			GameData.PAUSE_ACTIVE = true;
			bPause = true;
			this.parent.visible = false;
		}
		
		private function onUnpause(e:GameEvent):void 
		{
			
			GameData.PAUSE_ACTIVE = false;
			bPause = false;
			this.parent.visible = true;
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			GameDispatcher.instance.removeEventListener(GameEvent.PAUSE, onPause);
			GameDispatcher.instance.removeEventListener(GameEvent.UNPAUSE, onUnpause);
			GameDispatcher.instance.removeEventListener(GameEvent.END_GAME, onEndHandler);
			atlas = null;
			removeChildren();
			pauseButton.dispose();
			pauseButton = null;
			this.dispose();
		}
	}
}
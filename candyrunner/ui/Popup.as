package candyrunner.ui 
{
	import candyrunner.core.ILayer;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.SceneEvent;
	import candyrunner.player.HeroScore;
	import candyrunner.views.ViewConfig;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.TextureAtlas;
	
	public class Popup extends Sprite implements ILayer 
	{
		private var atlas:TextureAtlas;
		private var popupScreen:InterfaceObject;
		private var resumeButton:ClassButton;
		private var restartButton:ClassButton;
		private var menuButton:ClassButton;
		
		private var scoreTab:InterfaceObject;
		private var scoreBase:TextObject;
		private var scoreText:TextObject;
		
		private var highTab:InterfaceObject;
		private var highBase:TextObject;
		private var highText:TextObject;
		
		public function Popup(popupAtlas:TextureAtlas) 
		{
			super();
			this.atlas = popupAtlas;
			
			init();
		}
		
		private function init():void 
		{
			var tint:Quad = new Quad(GameData.SCREEN_WIDTH, GameData.SCREEN_HEIGHT, 0x000000);
			tint.alpha = 0.5;
			addChild(tint);
			
			popupScreen = new InterfaceObject(atlas.getTexture(TextureData.POPUP_SCREEN), true, GameData.SCREEN_WIDTH / 2, GameData.SCREEN_HEIGHT / 2);
			addChild(popupScreen);
			
			resumeButton = new ClassButton(popupScreen.x + 15, popupScreen.y - 60, "RESUME", atlas);
			addChild(resumeButton);
			
			restartButton = new ClassButton(popupScreen.x + 15, popupScreen.y + 20, "RESTART", atlas);
			addChild(restartButton);
			
			menuButton = new ClassButton(popupScreen.x + 15, popupScreen.y + 100, "QUIT", atlas);
			addChild(menuButton);
			
			//----------------
			scoreTab = new InterfaceObject(atlas.getTexture(TextureData.HIGHSCORE_TAB), false, popupScreen.x - 200, popupScreen.y - 250);
			addChild(scoreTab);
			scoreTab.visible = false;
			
			scoreBase = new TextObject(false, scoreTab.x + 85, scoreTab.y + 75, 250, 50, 36);
			scoreBase.text = "HIGHSCORE:";
			addChild(scoreBase);
			scoreBase.visible = false;
			
			scoreText = new TextObject(false, scoreBase.x + scoreBase.textBounds.width, scoreBase.y, 90, 50, 36, false);
			scoreText.text = getHighscore().toString();
			addChild(scoreText);
			scoreText.visible = false;
			
			//----------------
			highTab = new InterfaceObject(atlas.getTexture(TextureData.SCORE_TAB), false, popupScreen.x - 160, popupScreen.y - 185);
			addChild(highTab);
			
			highBase = new TextObject(false, highTab.x + 60, highTab.y + 20, 250, 50, 36);
			highBase.text = "SCORE:";
			addChild(highBase);
			
			highText = new TextObject(false, highBase.x + highBase.textBounds.width, highBase.y, 95, 50, 36, false);
			highText.text = GameData.CURRENT_SCORE.toString();
			addChild(highText);
			
			restartButton.addEventListener(TouchEvent.TOUCH, onTouch);
			menuButton.addEventListener(TouchEvent.TOUCH, onTouch);
			resumeButton.addEventListener(TouchEvent.TOUCH, onTouch);
			
			GameDispatcher.instance.addEventListener(GameEvent.DISABLE_UNPAUSE, onDisable);
		}
		
		private function getHighscore():int 
		{
			var highscores:Array = HeroScore.getHighscores();
			
			if (GameData.INIT_MULTIPLIER == GameData.INIT_EASY_MULTIPLIER) 
			{
				return highscores[0][0];
			}
			else if (GameData.INIT_MULTIPLIER == GameData.INIT_MEDIUM_MULTIPLIER) 
			{
				return highscores[1][0];
			}
			else
			{
				return highscores[2][0];
			}
		}
		
		private function onDisable(e:GameEvent):void 
		{
			if (resumeButton != null) 
			{
				resumeButton.removeEventListeners();
				resumeButton.visible = false;
				highTab.y = popupScreen.y - 100;
				highBase.y = highTab.y + 20;
				highText.y = highBase.y;
				scoreTab.visible = true;
				scoreBase.visible = true;
				scoreText.visible = true;
			}
		}
		
		private function onTouch(e:TouchEvent):void 
		{
			if (e.getTouch(restartButton, TouchPhase.BEGAN))
			{
				GameData.FILES_TO_LOAD = 1;
				popupScreen.visible = false;
				menuButton.visible = false;
				restartButton.visible = false;
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, GameData.CLICK);
				GameDispatcher.instance.dispatchEvent(new SceneEvent(ViewConfig.POPUP_VIEW, null));
				GameDispatcher.instance.dispatchEvent(new SceneEvent(ViewConfig.HUD_VIEW, null));
				GameDispatcher.instance.dispatchEvent(new SceneEvent(ViewConfig.GAME_VIEW, ViewConfig.GAME_VIEW, true));
			}
			else if (e.getTouch(menuButton, TouchPhase.BEGAN)) 
			{
				GameData.FILES_TO_LOAD = 1;
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, GameData.CLICK);
				GameDispatcher.instance.dispatchEvent(new SceneEvent(ViewConfig.POPUP_VIEW, null));
				GameDispatcher.instance.dispatchEvent(new SceneEvent(ViewConfig.HUD_VIEW, null));
				GameDispatcher.instance.dispatchEvent(new SceneEvent(ViewConfig.GAME_VIEW, ViewConfig.MENU_VIEW, true));
			}
			else if (e.getTouch(resumeButton, TouchPhase.BEGAN))
			{
				GameDispatcher.instance.dispatchEventWith("PLAY_SOUND", false, GameData.CLICK);
				GameDispatcher.instance.dispatchEvent(new SceneEvent(ViewConfig.POPUP_VIEW, ViewConfig.FADE_VIEW));
			}
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			GameDispatcher.instance.removeEventListener(GameEvent.DISABLE_UNPAUSE, onDisable);
			removeChildren();
			
			highText.removeMe();
			highText = null;
			highBase.removeMe();
			highBase = null;
			highTab.removeMe();
			highTab = null;
			
			scoreText.removeMe();
			scoreText = null;
			scoreBase.removeMe();
			scoreBase = null;
			scoreTab.removeMe();
			scoreTab = null;
			
			resumeButton.removeMe();
			resumeButton = null;
			popupScreen.removeMe();
			popupScreen = null;
			restartButton.removeMe();
			restartButton = null;
			menuButton.removeMe();
			menuButton = null;
			
			this.dispose();
		}
	}
}
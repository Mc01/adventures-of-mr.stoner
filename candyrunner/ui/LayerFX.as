package candyrunner.ui 
{
	import candyrunner.core.ILayer;
	import candyrunner.data.GameData;
	import candyrunner.level.ParticleEmitter;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public class LayerFX extends Sprite implements ILayer
	{
		[Embed(source = "../../../bin/assets/ghosting.pex", mimeType = "application/octet-stream")]
		private static const GhostingConfig:Class;
		
		[Embed(source = "../../../bin/assets/ghosting.png")]
		private static const GhostingPNG:Class;
		
		[Embed(source = "../../../bin/assets/explode.pex", mimeType = "application/octet-stream")]
		private static const ExplodeConfig:Class;
		
		private var ghostEmitter:ParticleEmitter;
		private var explodeEmitter:ParticleEmitter;
		private var dispatcher:GameDispatcher;
		
		public function LayerFX() 
		{
			dispatcher = GameDispatcher.instance;
			dispatcher.addEventListener(GameEvent.GHOST_READY, onGhostReady);
			dispatcher.addEventListener(GameEvent.GHOST_ON, onGhostOn);
			dispatcher.addEventListener(GameEvent.GHOST_OFF, onGhostOff);
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			ghostEmitter = new ParticleEmitter(XML(new GhostingConfig), Texture.fromBitmap(new GhostingPNG));
			explodeEmitter = new ParticleEmitter(XML(new ExplodeConfig), Texture.fromBitmap(new GhostingPNG));
			ghostEmitter.emitterX = 475;
			ghostEmitter.emitterY = GameData.SCREEN_HEIGHT - 53;
			explodeEmitter.emitterX = 800;
			explodeEmitter.emitterY = GameData.SCREEN_HEIGHT - 38;
		}
		
		private function onGhostReady(e:GameEvent):void 
		{
			Starling.juggler.add(ghostEmitter);
			addChild(ghostEmitter);
			ghostEmitter.start();
		}
		
		private function onGhostOn(e:GameEvent):void 
		{
			ghostEmitter.stop();
            Starling.juggler.remove(ghostEmitter);
            removeChild(ghostEmitter);
			
			Starling.juggler.add(explodeEmitter);
			addChild(explodeEmitter);
			explodeEmitter.start();
			
			dispatcher.dispatchEventWith("PLAY_SOUND", false, GameData.GHOST_FIRE);
		}
		
		private function onGhostOff(e:GameEvent):void 
		{
			explodeEmitter.stop();
            Starling.juggler.remove(explodeEmitter);
            removeChild(explodeEmitter);
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			dispatcher.removeEventListener(GameEvent.GHOST_READY, onGhostReady);
			dispatcher.removeEventListener(GameEvent.GHOST_ON, onGhostOn);
			dispatcher.removeEventListener(GameEvent.GHOST_OFF, onGhostOff);
			
			if (explodeEmitter.isEmitting) 
			{
				explodeEmitter.stop();
				Starling.juggler.remove(explodeEmitter);
			}
			if (ghostEmitter.isEmitting) 
			{
				
				ghostEmitter.stop();
				Starling.juggler.remove(ghostEmitter);
			}
			
			removeChildren();
			
			explodeEmitter.clear();
			explodeEmitter.dispose();
			ghostEmitter.clear();
			ghostEmitter.clear();
			
			dispatcher = null;
			explodeEmitter = null;
			ghostEmitter = null;
			
			this.dispose();
		}
	}
}
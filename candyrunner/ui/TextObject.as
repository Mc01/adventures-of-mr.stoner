package candyrunner.ui 
{
	import candyrunner.core.ILayer;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class TextObject extends TextField implements ILayer 
	{
		
		public function TextObject(pivotSwitch:Boolean, x:int, y:int, width:int, height:int, size:int = 38, alignLeft:Boolean = true) 
		{
			super(width, height, "", "Rustler", size, 0xFFFFFF, false);
			if (pivotSwitch) 
			{
				this.pivotX = this.width / 2;
				this.pivotY = this.height / 2;
			}
			this.x = x;
			this.y = y;
			this.vAlign = VAlign.TOP;
			if (alignLeft) 
			{
				this.hAlign = HAlign.LEFT;
			}
			else 
			{
				this.hAlign = HAlign.RIGHT;
			}
			this.touchable = false;
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			this.dispose();
		}
	}
}
package candyrunner.ui 
{
	import candyrunner.core.GameObject;
	import candyrunner.core.ILayer;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import flash.geom.Rectangle;
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class ProgressBar extends Sprite implements ILayer
	{	
		private var maskRectangle:Rectangle;
		private var progressImage:Image;
		private var atlas:TextureAtlas;
		private var ghostImage:Image;
		
		public function ProgressBar(atlas:TextureAtlas, x:int, y:int) 
		{
			super();
			this.atlas = atlas;
			this.x = x;
			this.y = y;
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			progressImage = new Image(atlas.getTexture(TextureData.PROGRESS_BAR));
			addChild(progressImage);
			ghostImage = new Image(atlas.getTexture(TextureData.PROGRESS_BAR_GHOSTING));
			ghostImage.visible = false;
			addChild(ghostImage);
			maskRectangle = new Rectangle(x, y, 0, progressImage.height);
			
			GameDispatcher.instance.addEventListener(GameEvent.GHOST_ON, onGhostOn);
			GameDispatcher.instance.addEventListener(GameEvent.GHOST_OFF, onGhostOff);
		}
		
		private function onGhostOn(e:GameEvent):void 
		{
			ghostImage.visible = true;
		}
		
		private function onGhostOff(e:GameEvent):void 
		{
			ghostImage.visible = false;
		}
		
		override public function render(support:RenderSupport, parentAlpha:Number):void 
		{
			support.finishQuadBatch()
			Starling.context.setScissorRectangle(maskRectangle);
			super.render(support, parentAlpha);
			support.finishQuadBatch()
			Starling.context.setScissorRectangle(null);
		}
		
		public function changeWidth(width:int):void 
		{
			maskRectangle.width = width;
		}
		
		public function removeMe():void 
		{
			GameDispatcher.instance.removeEventListener(GameEvent.GHOST_ON, onGhostOn);
			GameDispatcher.instance.removeEventListener(GameEvent.GHOST_OFF, onGhostOff);
			
			atlas = null;
			removeChildren();
			ghostImage.dispose();
			progressImage.dispose();
			ghostImage = null;
			progressImage = null;
			maskRectangle = null;
			this.dispose();
		}
	}
}
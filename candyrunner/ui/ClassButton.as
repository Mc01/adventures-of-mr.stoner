package candyrunner.ui 
{
	import candyrunner.core.ILayer;
	import candyrunner.data.TextureData;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class ClassButton extends Sprite implements ILayer 
	{	
		private var text:String;
		private var atlas:TextureAtlas;
		private var buttonObject:InterfaceObject;
		private var textObject:TextObject;
		
		public function ClassButton(x:int, y:int, text:String, atlas:TextureAtlas) 
		{
			this.x = x;
			this.y = y;
			this.text = text;
			this.atlas = atlas;
			init();
		}
		
		private function init():void 
		{
			buttonObject = new InterfaceObject(atlas.getTexture(TextureData.CLASS_BUTTON), true, 0, 0);
			addChild(buttonObject);
			
			textObject = new TextObject(true, 0, buttonObject.height / 4, buttonObject.width, buttonObject.height);
			textObject.text = text;
			textObject.x = (buttonObject.width - textObject.textBounds.width) / 2 - 2;
			addChild(textObject);
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			atlas = null;
			removeChildren();
			textObject.removeMe();
			buttonObject.removeMe();
			textObject = null;
			buttonObject = null;
			this.dispose();
		}
	}
}
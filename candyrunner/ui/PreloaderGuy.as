package candyrunner.ui 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import Preloader.GamePreloader;
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	
	
	public class PreloaderGuy extends Sprite 
	{
		
		private var maskRectangle:Rectangle;
		private var progressImage:Sprite;
		
		public function PreloaderGuy(x:int, y:int) 
		{
			this.x = x;
			this.y = y;
			var preloader:GamePreloader = new GamePreloader();
			progressImage = getSpriteFromSWC(preloader, "Bar");
			addChild(progressImage);
			maskRectangle = new Rectangle(x, y, 324, 603);
		}
				
		
		override public function render(support:RenderSupport, parentAlpha:Number):void 
		{
			support.finishQuadBatch()
			Starling.context.setScissorRectangle(maskRectangle);
			super.render(support, parentAlpha);
			support.finishQuadBatch()
			Starling.context.setScissorRectangle(null);
		}
		
		
		public function changeHeight(height:int):void 
		{
			maskRectangle.height = height;
		}
		
		private function getSpriteFromSWC(swcContent:Object, childName:String):Sprite
		{
			var helperSprite:flash.display.Sprite = swcContent.getChildByName(childName) as flash.display.Sprite;
			var bitmapData:BitmapData = new BitmapData(helperSprite.width, helperSprite.height, true, 0x00FFFFFF);
			bitmapData.draw(helperSprite);
			var bitmap:Bitmap = new Bitmap(bitmapData, "auto", false)
			
			var texture:Texture = Texture.fromBitmap(Bitmap(bitmap));
			var objectImage:Image = new Image(texture);
			var sprite:Sprite = new Sprite();
			sprite.addChild(objectImage);
			return sprite;
		}
		
	}

}
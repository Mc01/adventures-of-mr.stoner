package candyrunner.ui 
{
	import candyrunner.core.GameObject;
	import candyrunner.core.ILayer;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import candyrunner.main.SceneEvent;
	import candyrunner.views.ViewConfig;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class Countdown extends GameObject implements ILayer 
	{
		private var pauseTimer:Timer;
		
		public function Countdown(fadeAtlas:TextureAtlas) 
		{
			super(extractVector(fadeAtlas, TextureData.COUNTDOWN));
			this.x = (GameData.SCREEN_WIDTH + x) / 2;
			this.y = (GameData.SCREEN_HEIGHT + y + this.height) / 2;
		}
		
		private function extractVector(atlas:TextureAtlas, nameArray:Array):Vector.<Texture> 
		{
			var vector:Vector.<Texture> = new Vector.<Texture>();
			
			for each (var element:String in nameArray) 
			{
				vector.push(atlas.getTexture(element));
			}
			
			return vector;
		}
		
		public function startCountdown():void 
		{
			currentFrame = 0;
			pauseTimer = new Timer(600, 4);
			pauseTimer.addEventListener(TimerEvent.TIMER, onTimer);
			pauseTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
			pauseTimer.start();
		}
		
		private function onTimer(e:TimerEvent):void 
		{
			if (currentFrame < numFrames - 1) 
			{
				currentFrame++;
				if (currentFrame == numFrames - 1) 
				{
					GameDispatcher.instance.dispatchEvent(new GameEvent(GameEvent.UNPAUSE));
				}
			}
		}
		
		private function onTimerComplete(e:TimerEvent):void 
		{
			if (pauseTimer != null) 
			{
				pauseTimer.removeEventListener(TimerEvent.TIMER, onTimer);
				pauseTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
				pauseTimer = null;
			}
			GameDispatcher.instance.dispatchEvent(new SceneEvent(ViewConfig.FADE_VIEW, null));
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			if (pauseTimer != null) 
			{
				pauseTimer.removeEventListener(TimerEvent.TIMER, onTimer);
				pauseTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
				pauseTimer = null;
			}
			this.dispose();
		}	
	}
}
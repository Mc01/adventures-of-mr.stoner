package candyrunner.ui 
{
	import candyrunner.core.ILayer;
	import candyrunner.core.IUpdateableObject;
	import candyrunner.data.GameData;
	import candyrunner.data.PlayerData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import candyrunner.main.GameEvent;
	import flash.geom.Rectangle;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class HUD extends Sprite implements ILayer
	{
		private var scoreField:TextObject;
		private var progressBar:ProgressBar;
		private var atlas:TextureAtlas;
		private var dispatcher:GameDispatcher;
		private var pauseButton:PauseButton;
		private var progressBase:InterfaceObject;
		private var progressOver:InterfaceObject;
		private var progressGhost:InterfaceObject;
		private var scoreTab:InterfaceObject;
		private var scoreBase:TextObject;
		private var layerFX:LayerFX;
		
		public function HUD(atlas:TextureAtlas) 
		{
			super();
			this.atlas = atlas;
			dispatcher = GameDispatcher.instance;			
			addEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
		}	
		
		private function onAddedHandler(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
			init();
		}
		
		private function init():void 
		{		
			scoreTab = new InterfaceObject(atlas.getTexture(TextureData.SCORE_SMALL), false, GameData.SCREEN_WIDTH - 320, 10);
			addChild(scoreTab);
			
			scoreBase = new TextObject(false, scoreTab.x + 38, scoreTab.y + 15, 250, 50, 36);
			scoreBase.text = "SCORE:";
			addChild(scoreBase);
			
			scoreField = new TextObject(false, scoreBase.x + scoreBase.textBounds.width, scoreBase.y, 95, 50, 36, false);
			addChild(scoreField);
			
			pauseButton = new PauseButton(atlas, 35, 5);
			addChild(pauseButton);
			
			progressBase = new InterfaceObject(atlas.getTexture(TextureData.PROGRESS_BASE), false, 150, GameData.SCREEN_HEIGHT - 70);
			addChild(progressBase);
			
			progressBar = new ProgressBar(atlas, progressBase.x + 54, progressBase.y + 25);
			addChild(progressBar);
			
			progressOver = new InterfaceObject(atlas.getTexture(TextureData.PROGRESS_OVER), false, progressBase.x + 83, progressBase.y + 12);
			addChild(progressOver);
			
			layerFX = new LayerFX();
			addChild(layerFX);
			
			progressGhost = new InterfaceObject(atlas.getTexture(TextureData.PROGRESS_DUCH), false, progressBase.x + 625, progressBase.y + 10);
			progressGhost.visible = false;
			addChild(progressGhost);
			
			dispatcher.addEventListener(GameEvent.UPDATE_HUD, onUpdate);
			dispatcher.addEventListener(GameEvent.GHOST_READY, onReady);
			dispatcher.addEventListener(GameEvent.GHOST_OFF, onOff);
		}
		
		private function onUpdate(e:GameEvent):void 
		{
			GameData.CURRENT_SCORE = GameData.CAUGHT_COIN_NUMBER * GameData.COIN_POINT_NUMBER + int(PlayerData.PLAYER_DISTANCE / GameData.ONE_METER_IN_PIXELS);
			var score:int = GameData.CURRENT_SCORE;
			scoreField.text = score.toString();
			
			var progress:int = 1 + (progressBar.width - 1) * GameData.GHOST_COIN_NUMBER / GameData.GHOST_READY_NUMBER;
			progressBar.changeWidth(progress);
			
			progressGhost.y = progressBase.y + 10 + Math.sin(GameData.RADIAN/5)*3;
		}
		
		private function onReady(e:GameEvent):void 
		{
			progressGhost.visible = true;
		}
		
		private function onOff(e:GameEvent):void 
		{
			progressGhost.visible = false;
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			dispatcher.removeEventListener(GameEvent.UPDATE_HUD, onUpdate);
			dispatcher.removeEventListener(GameEvent.GHOST_READY, onReady);
			dispatcher.removeEventListener(GameEvent.GHOST_OFF, onOff);
			removeChildren();
			
			layerFX.removeMe();
			pauseButton.removeMe();
			progressGhost.removeMe();
			progressOver.removeMe();
			progressBar.removeMe();
			progressBase.removeMe();
			scoreTab.removeMe();
			scoreBase.removeMe();
			scoreField.removeMe();
			
			atlas = null;
			layerFX = null;
			pauseButton = null;
			progressGhost = null;
			progressOver = null;
			progressBar = null;
			progressBase = null;
			scoreTab = null;
			scoreBase = null;
			scoreField = null;
			
			this.dispose();
		}
	}
}
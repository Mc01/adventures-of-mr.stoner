package candyrunner.ui 
{
	import candyrunner.core.GameObject;
	import candyrunner.core.ILayer;
	import candyrunner.core.IScreen;
	import candyrunner.data.GameData;
	import candyrunner.data.TextureData;
	import candyrunner.main.GameDispatcher;
	import starling.display.Sprite;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class MuteWrapper extends Sprite implements IScreen, ILayer
	{
		private var mute:GameObject;
		
		public function MuteWrapper(atlas:TextureAtlas, x:int, y:int) 
		{
			this.x = x;
			this.y = y;
			
			var muteVector:Vector.<Texture> = new Vector.<Texture>();
			for each (var element:String in TextureData.SOUND_VOLUME) 
			{
				muteVector.push(atlas.getTexture(element));
			}
			
			mute = new GameObject(muteVector);
			addChild(mute);
		}
		
		/* INTERFACE candyrunner.core.IScreen */
		
		public function disable():void 
		{
			mute.removeEventListeners();
		}
		
		public function enable():void 
		{
			mute.currentFrame = GameData.SOUNDS_VOLUME;
			mute.addEventListener(TouchEvent.TOUCH, onTouchHandler);
		}
		
		private function onTouchHandler(e:TouchEvent):void 
		{
			if (e.getTouch(mute, TouchPhase.BEGAN)) 
			{
				if (GameData.SOUNDS_VOLUME == 0) 
				{
					GameDispatcher.instance.dispatchEventWith("UNMUTE");
				}
				else 
				{
					GameDispatcher.instance.dispatchEventWith("MUTE");
				}
				mute.currentFrame = GameData.SOUNDS_VOLUME;
			}
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			removeChildren();
			mute.dispose();
			mute = null;
			this.dispose();
		}
	}
}
package candyrunner.ui 
{
	import candyrunner.core.ILayer;
	import starling.display.Image;
	import starling.textures.Texture;
	
	public class InterfaceObject extends Image implements ILayer 
	{
		public function InterfaceObject(texture:Texture, pivotSwitch:Boolean, x:int, y:int) 
		{
			super(texture);
			if (pivotSwitch) 
			{
				this.pivotX = this.width / 2;
				this.pivotY = this.height / 2;
			}
			this.x = x;
			this.y = y;
		}
		
		/* INTERFACE candyrunner.core.ILayer */
		
		public function removeMe():void 
		{
			this.dispose();
		}
	}
}
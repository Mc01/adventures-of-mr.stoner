package candyrunner
{
	import candyrunner.data.GameData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.utils.getDefinitionByName;
	import Preloader.GamePreloader;
	
	
	public class Preloader extends MovieClip 
	{
		private var _gamePreloader:GamePreloader;
		private var bar:Sprite;
		private var background:Sprite;
		
		public function Preloader() 
		{
			if (stage) {
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = StageAlign.TOP_LEFT;
			}
			addEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			// TODO show loader
			GameData.FILES_TO_LOAD = 2;
			GameData.FILES_LOADED = 0;
			_gamePreloader = new GamePreloader();
			addChild(_gamePreloader);
			
			background = _gamePreloader.getChildByName('Background') as Sprite;
			bar = _gamePreloader.getChildByName('Bar') as Sprite;
			bar.width = 0;
		}
		
		private function ioError(e:IOErrorEvent):void 
		{
			trace(e.text);
		}
		
		private function progress(e:ProgressEvent):void 
		{
			// TODO update loader
			
			
		}
		
		private function checkFrame(e:Event):void 
		{
			if (currentFrame == totalFrames) 
			{
				stop();
				loadingFinished();
			}
		}
		
		private function loadingFinished():void 
		{
			removeEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			// TODO hide loader
			//GameData.FILES_LOADED++;
			
			startup();
		}
		
		private function startup():void 
		{
			var mainClass:Class = getDefinitionByName("candyrunner.Main") as Class;
			addChild(new mainClass(_gamePreloader) as DisplayObject);
		}
	}
}
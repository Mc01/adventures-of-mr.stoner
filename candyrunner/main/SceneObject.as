package candyrunner.main 
{
	import starling.textures.TextureAtlas;
	public class SceneObject
	{
		private var _unload:String;
		private var _load:String;
		private var _usePreloader:Boolean;
		
		public function SceneObject(unload:String, load:String, usePreloader:Boolean) 
		{
			_unload = unload;
			_load = load;
			_usePreloader = usePreloader;
		}
		
		public function get unload():String 
		{
			return _unload;
		}
		
		public function get load():String 
		{
			return _load;
		}
		
		public function get usePreloader():Boolean 
		{
			return _usePreloader;
		}
	}
}
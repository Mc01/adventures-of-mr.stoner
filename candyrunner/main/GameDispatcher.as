package candyrunner.main 
{
	import starling.events.EventDispatcher;
	
	public class GameDispatcher extends EventDispatcher 
	{
		private static var _instance:GameDispatcher;
		
		public function GameDispatcher() 
		{
			super();
			_instance = this;
		}
		
		static public function get instance():GameDispatcher 
		{
			return _instance;
		}
	}
}
package candyrunner.main 
{
	import starling.events.Event;
	
	public class GameEvent extends Event 
	{
		public static const CLEANER_HIT:String = "cleaner_remove_obstacle_from_hit";
		public static const CLEANER_PARALAX:String = "cleaner_remove_obstacle_from_paralax";
		public static const CLEANER_POWER:String = "cleaner_remove_obstacle_from_power";
		public static const CLEANER_LEVEL:String = "cleaner_remove_obstacle_from_level";
		
		public static const COIN_PARTICLES:String = "coin_particles";
		public static const POWER_INIT:String = "hero_caught_power_orb";
		public static const POWER_COURIER:String = "coins_from_paralax_to_power";
		public static const POWER_FINISH:String = "power_finish";
		
		public static const REMOVE_PRELOADER:String = "remove_preloader";
		public static const CHANGE_SCREEN:String = "change_screen_inside_menu_screens";
		public static const UPDATE_HUD:String = "update_hud";
		
		public static const DISABLE_UNPAUSE:String = "disable_unpause";
		public static const PAUSE:String = "pause";
		public static const UNPAUSE:String = "unpause";
		public static const RESTART:String = "restart";
		
		public static const END_GAME:String = "end_game";
		public static const ATLAS_FOR_GAME:String = "atlas_game";
		public static const ATLAS_FOR_MENU:String = "atlas_menu";
		public static const ONCE_AGAIN_ATLAS:String = "atlas_again";
		
		public static const GHOST_READY:String = "ghost_ready";
		public static const GHOST_ON:String = "ghost_on";
		public static const GHOST_OFF:String = "ghost_off";
		
		public function GameEvent(type:String, data:Object=null) 
		{
			super(type, false, data);
		}
	}
}
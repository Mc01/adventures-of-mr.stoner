package candyrunner.main 
{
	import starling.textures.TextureAtlas;
	public class SceneEvent extends GameEvent 
	{
		public static const CHANGE_SCENE:String = "change_scene";
		
		public function SceneEvent(unload:String, load:String, usePreloader:Boolean = false) 
		{
			var sceneData:SceneObject = new SceneObject(unload, load, usePreloader);
			super(CHANGE_SCENE, sceneData);
		}
	}
}
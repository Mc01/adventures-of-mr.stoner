package candyrunner.main
{	
	import candyrunner.assets.AssetsPaths;
	import candyrunner.assets.AtlasManager;
	import candyrunner.data.GameData;
	import candyrunner.loader.AssetsLoader;
	import candyrunner.loader.Components;
	import candyrunner.screens.GameLayers;
	import candyrunner.screens.SceneManager;
	import candyrunner.sound.SoundAssets;
	import candyrunner.sound.SoundManager;
	import candyrunner.views.ViewConfig;
	import flash.display.Bitmap;
	import flash.media.Sound;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	
	public class GameManager extends Sprite
	{
		private var stopFlag:Boolean;
		private var gameLayers:GameLayers;
		private var sceneManager:SceneManager;
		private var gameDispatcher:GameDispatcher;		
		private var atlasManager:AtlasManager;
		private var soundManager:SoundManager;
		
		
		public function GameManager()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
		}		
		
		private function init():void 
		{
			gameDispatcher = new GameDispatcher();	
			gameDispatcher.addEventListener(ViewConfig.FONTS, onLoaded)

			gameLayers = new GameLayers();
			addChild(gameLayers);
			
			atlasManager = new AtlasManager();
			
			sceneManager = new SceneManager(gameLayers);
			gameDispatcher.dispatchEvent(new SceneEvent(null, ViewConfig.FONTS, true));
			
			soundManager = new SoundManager();
			soundManager.addSound(Sound(new SoundAssets.GAME_BACKGROUND()), GameData.GAME_BACKGROUND);
			soundManager.addSound(Sound(new SoundAssets.MENU_BACKGROUND()), GameData.MENU_BACKGROUND);
			soundManager.addSound(Sound(new SoundAssets.SOUND_COIN()), GameData.COIN_COLLECT);
			soundManager.addSound(Sound(new SoundAssets.SOUND_JUMP()), GameData.JUMP);
			soundManager.addSound(Sound(new SoundAssets.SOUND_GRAVITY()), GameData.GRAVITY);
			soundManager.addSound(Sound(new SoundAssets.CLICK()), GameData.CLICK);
			soundManager.addSound(Sound(new SoundAssets.WHOOSH()), GameData.GHOST_FIRE);			
		}	
		
		private function onAddedHandler(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedHandler);
			init();
		}
		
		private function onLoaded(e:Event):void 
		{
			var array:Array = e.data as Array;
			var bitmap:Bitmap = Bitmap(array[0]);
			var xml:XML = XML(array[1]);
			var texture:Texture = Texture.fromBitmap(Bitmap(bitmap));
			TextField.registerBitmapFont(new BitmapFont(texture, xml));
			gameDispatcher.dispatchEvent(new SceneEvent(null, ViewConfig.MENU_VIEW, true));
		}
	}
}
package candyrunner.swiper 
{
	/**
	 * ...
	 * @author Andy
	 */
	public class SwipeDirection 
	{
		static public const LEFT:String = "left";
		static public const RIGHT:String = "right";
		static public const UP:String = "up";
		static public const DOWN:String = "down";
	}

}
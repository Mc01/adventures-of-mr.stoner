package candyrunner.swiper 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author Andy
	 */
	public class SwipeParams 
	{
		public var direction:String;
		public var directionVector:Point;
		public var pixelDistanceX:Number;
		public var pixelDistanceY:Number;
		public var progressX:Number;
		public var progressY:Number;
		public var velocityY:Number;
		public var velocityX:Number; 
		
		public function SwipeParams()
		{
			
		}
		
		public function get velocity():Number 
		{
			var vel:Number = (velocityX + velocityY) / 2;
			
			if (!isFinite(vel) || isNaN(vel))
				vel = 0
			
			return vel;
		}
	}

}
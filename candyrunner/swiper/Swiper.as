package candyrunner.swiper 
{
	import flash.geom.Rectangle;
	import starling.display.DisplayObject;
	import starling.display.Stage;
	import flash.geom.Point;
	import flash.utils.getTimer;
	import starling.events.EventDispatcher;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	/**
	 * ...
	 * @author Andy
	 */
	public class Swiper extends EventDispatcher
	{
		private var _stage:DisplayObject;
		private var _startPoint:Point;
		private var _pixelDistanceX:Number;
		private var _pixelDistanceY:Number;
		private var _targetDistanceX:Number;
		private var _targetDistanceY:Number;
		private var _progressX:Number;
		private var _progressY:Number;
		private var _direction:String;
		private var _velocityX:Number;
		private var _velocityY:Number;
		private var _progressScope:Number = 1;
		private var _timeStamp:int;
		private var _swipeStatus:String;
		private var _resetSwipe:Boolean = false;
		private var _lastLocation:Point;
		private var _bounds:Rectangle;
		private var _startThreshold:Number = 0;
		private var _swipeStarted:Boolean = false;
		private var dir:Point;
		
		public function Swiper(stage:DisplayObject, bounds:Rectangle) 
		{
			_stage = stage;	
			_bounds = bounds;
			
			_stage.addEventListener(TouchEvent.TOUCH, touchHandler);
		}
		
		public function enable():void
		{
			_stage.addEventListener(TouchEvent.TOUCH, touchHandler);
		}
		
		public function disable():void
		{
			_stage.removeEventListener(TouchEvent.TOUCH, touchHandler);
		}
		
		private function touchHandler(e:TouchEvent):void 
		{
			var touch:Touch = e.getTouch(_stage);		
			
			if (!touch)
				return;
								
			var touchLocation:Point = touch.getLocation(_stage);
					
			switch (touch.phase)
			{
				case TouchPhase.BEGAN:
					if (!_bounds.containsPoint(touchLocation))	
						return;
						
					startSwipe(touchLocation);
					break;
				case TouchPhase.ENDED:
					endSwipe();
					break;
				case TouchPhase.MOVED:
					if (!_bounds.containsPoint(touchLocation))	
						return;
						
					if (_resetSwipe)
					{
						startSwipe(touchLocation)
						_resetSwipe = false;
					}
					
					progressSwipe(touchLocation);
					break;
			}
		}
		
		private function progressSwipe(touchLocation:Point):void 
		{	
			if (_swipeStatus != TouchPhase.BEGAN)
				return;
			
			//var swipeStarted:Boolean = _direction == "" ? true : false;
					
			_pixelDistanceX = touchLocation.x - _startPoint.x;
			_pixelDistanceY = touchLocation.y - _startPoint.y;
						
			dir = touchLocation.subtract(_lastLocation);
			dir.normalize(1);		
			
			var newDirection:String = "";
			
			if (dir.x < 0.1 && dir.x > -0.1)
			{
				if (dir.y > 0)
					newDirection = SwipeDirection.DOWN;
				else if (dir.y < 0)
					newDirection = SwipeDirection.UP;
				else
					newDirection = _direction;
			}
			else if (dir.y < 0.1 && dir.y > -0.1)
			{
				if (dir.x > 0)
					newDirection = SwipeDirection.RIGHT;
				else if (dir.x < 0)
					newDirection = SwipeDirection.LEFT;
				else
					newDirection = _direction;
			}
			else
				newDirection = _direction;
			
			//if ((_direction == SwipeDirection.LEFT && newDirection == SwipeDirection.RIGHT) ||
				//(_direction == SwipeDirection.RIGHT && newDirection == SwipeDirection.LEFT) ||
				//(_direction == SwipeDirection.UP && newDirection == SwipeDirection.DOWN) ||
				//(_direction == SwipeDirection.DOWN && newDirection == SwipeDirection.UP))
			//{
				//resetSwipe();
				//return;
			//}
			
			if (_direction != newDirection)
			{
				//_startPoint = touchLocation.clone();
				
				switch (newDirection)
				{
					case SwipeDirection.LEFT:
						_targetDistanceX = _startPoint.x;
						break;
					case SwipeDirection.RIGHT:
						_targetDistanceX = _bounds.width - _startPoint.x;
						break;
					case SwipeDirection.UP:
						_targetDistanceY = _startPoint.y;
						break;
					case SwipeDirection.DOWN:
						_targetDistanceY = _bounds.height - _startPoint.y;
						break;
				}
			}
			
			_direction = newDirection;
			
			var dirToOrigin:Point = touchLocation.subtract(_startPoint);
			dirToOrigin.normalize(1);
			dirToOrigin.x *= _bounds.width;
			dirToOrigin.y *= _bounds.height;
						
			_progressX = _pixelDistanceX / _targetDistanceX;
			_progressY = _pixelDistanceY / _targetDistanceY;			
			
			var distanceFromLastLocation:Number = 0;		
				
			var timeStamp:int = getTimer();
			var timeDifference:int = timeStamp - _timeStamp;
			
			if (timeDifference < 10)
				timeDifference = 10;
									
			//if (_direction == SwipeDirection.LEFT || _direction == SwipeDirection.RIGHT)
				_velocityX = Math.abs(_lastLocation.x - touchLocation.x) / timeDifference;
			//else if (_direction == SwipeDirection.UP || _direction == SwipeDirection.DOWN)
				_velocityY = Math.abs(_lastLocation.y - touchLocation.y) / timeDifference;
			
			if (!isFinite(_velocityX) || isNaN(_velocityX))
				_velocityX = 0;
				
			if (!isFinite(_velocityY) || isNaN(_velocityY))
				_velocityY = 0;
				
			//trace();
			//trace("SWIPE direction:", _direction);
			//trace("SWIPE distance:", _pixelDistance);
			//trace("SWIPE distance2:", distanceFromLastLocation);
			//trace("SWIPE progressX:", _progressX);
			//trace("SWIPE progressY:", _progressY);
			//trace("SWIPE velocity:", _velocity);
			
			if (Math.abs(_progressX) < _startThreshold && Math.abs(_progressY) < _startThreshold)
				return;
			
			var params:SwipeParams = new SwipeParams();
			params.direction = _direction;
			params.directionVector = dir;
			params.pixelDistanceX = _pixelDistanceX;
			params.pixelDistanceY = _pixelDistanceY;
			params.progressX = _progressX * _progressScope;
			params.progressY = _progressY * _progressScope;
			params.velocityX = _velocityX;
			params.velocityY = _velocityY;
			
			if (!_swipeStarted)
			{
				dispatchEvent(new SwipeEvent(SwipeEvent.SWIPE_STARTED, false, params));
				_swipeStarted = true;
			}
			else
			{
				dispatchEvent(new SwipeEvent(SwipeEvent.SWIPE_PROGRESS, false, params));
			}
				
			_lastLocation = touchLocation;
			_timeStamp = timeStamp;
		}
		
		private function startSwipe(startPoint:Point):void 
		{
			_swipeStatus = TouchPhase.BEGAN;		
			
			_startPoint = startPoint;
			_targetDistanceX = _startPoint.x > _bounds.width / 2 ? _bounds.width / 2 - _startPoint.x : _startPoint.x;
			_targetDistanceY = _startPoint.y > _bounds.height / 2 ? _bounds.height / 2 - _startPoint.y : _startPoint.y;
			_timeStamp = getTimer();
			_pixelDistanceX = 0;
			_pixelDistanceY = 0;
			_progressX = 0;
			_progressY = 0;
			_velocityX = 0;
			_velocityY = 0;
			_direction = "";
			_lastLocation = startPoint;
		}
		
		private function endSwipe():void 
		{
			_swipeStatus = TouchPhase.ENDED;
			
			_swipeStarted = false;
			
			var params:SwipeParams = new SwipeParams();
			params.direction = _direction;
			params.directionVector = dir;
			params.pixelDistanceX = _pixelDistanceX;
			params.pixelDistanceY = _pixelDistanceY;
			params.progressX = _progressX * _progressScope;
			params.progressY = _progressY * _progressScope;
			params.velocityX = _velocityX;
			params.velocityY = _velocityY;
			
			dispatchEvent(new SwipeEvent(SwipeEvent.SWIPE_ENDED, false, params));			
			
			_timeStamp = 0;
			_startPoint = null;
			_pixelDistanceX = 0;
			_pixelDistanceY = 0;
			_progressX = 0;
			_progressY = 0;
			_velocityX = 0;
			_velocityY = 0;
			_direction = "";
			_lastLocation = null;
		}
		
		public function clean():void
		{
			_stage.removeEventListener(TouchEvent.TOUCH, touchHandler);
			_stage = null;
		}
		
		public function resetSwipe():void 
		{
			_timeStamp = 0;
			_startPoint = null;
			//_pixelDistance = 0;
			//_progress = 0;
			_velocityX = 0;
			_velocityY = 0;
			_direction = "";
			_lastLocation = null;
			
			_resetSwipe = true;
		}
		
		public function forceEndSwipe():void 
		{
			_stage.touchable = false;
			endSwipe();
			_stage.touchable = true;
		}
		
		public function get height():Number 
		{
			return _bounds ? _bounds.height : 0;
		}
		
		public function set height(value:Number):void 
		{
			if (_bounds)
				_bounds.height = value;
		}
		
		public function get width():Number 
		{
			return _bounds ? _bounds.width : 0;
		}
		
		public function set width(value:Number):void 
		{
			if (_bounds)
				_bounds.width = value;
		}
		
		public function set startThreshold(value:Number):void 
		{
			_startThreshold = value;
		}
		
		public function set progressScope(value:Number):void 
		{	
			if (value > 1)
				value = 1;
			else if (value < -1)
				value = -1;
			
			_progressScope = value;
		}
		
	}

}
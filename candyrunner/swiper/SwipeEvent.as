package candyrunner.swiper 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Andy
	 */
	public class SwipeEvent extends Event 
	{
		static public const SWIPE_STARTED:String = "swipeStarted";
		static public const SWIPE_ENDED:String = "swipeEnded";
		static public const SWIPE_PROGRESS:String = "swipeProgress";
		
		public function SwipeEvent(type:String, bubbles:Boolean=false, data:Object=null) 
		{
			super(type, bubbles, data);			
		}
		
	}

}
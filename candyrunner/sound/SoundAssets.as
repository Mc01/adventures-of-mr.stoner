package candyrunner.sound 
{
	
	public class SoundAssets 
	{
		[Embed(source="../../../bin/assets/sounds/anything.mp3")]
		public static const GAME_BACKGROUND:Class;
		
		[Embed(source="../../../bin/assets/sounds/boing.mp3")]
		public static const MENU_BACKGROUND:Class;
		
		[Embed(source = "../../../bin/assets/sounds/coin.mp3")]
		public static const SOUND_COIN:Class;
		
		[Embed(source = "../../../bin/assets/sounds/jump.mp3")]
		public static const SOUND_JUMP:Class;
		
		[Embed(source = "../../../bin/assets/sounds/gravity.mp3")]
		public static const SOUND_GRAVITY:Class;
		
		[Embed(source="../../../bin/assets/sounds/click.mp3")]
		public static const CLICK:Class;
		
		[Embed(source = "../../../bin/assets/sounds/whoosh.mp3")]
		public static const WHOOSH:Class;
	}
}
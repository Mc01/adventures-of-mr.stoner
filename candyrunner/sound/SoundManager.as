package candyrunner.sound
{
	import candyrunner.data.GameData;
	import candyrunner.main.GameDispatcher;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;

	public class SoundManager
	{
		public function SoundManager()
		{
			sounds = new Dictionary();
			channels = new Dictionary();
			
			GameDispatcher.instance.addEventListener("PLAY_SOUND", onPlaySound);
			GameDispatcher.instance.addEventListener("PLAY_BACKGROUND", onPlayBackground);
			GameDispatcher.instance.addEventListener("MUTE", onMute);
			GameDispatcher.instance.addEventListener("UNMUTE", onUnmute);
		}
		
		private function onUnmute(e:Object):void 
		{
			SoundMixer.soundTransform = new SoundTransform(1);
			GameData.SOUNDS_VOLUME = 1;
		}
		
		private function onMute(e:Object):void 
		{
			SoundMixer.soundTransform = new SoundTransform(0);
			GameData.SOUNDS_VOLUME = 0;
		}
		
		private function onPlayBackground(e:Object):void 
		{
			stopAll();
			playSound(String(e.data), int.MAX_VALUE);
		}
		
		private function onPlaySound(e:Object):void 
		{
			playSound(String(e.data));
		}
				
		public function addSound(element:Sound, name:String):void
		{
			if(sounds[name] == null)
				sounds[name] = element;
		}
		
		public function playSound(name:String, loops:int = 1, volume:Number = 1):void
		{
			channels[name] = Sound(sounds[name]).play(0,loops,new SoundTransform(volume, 0));
		}
		
		public function stopAll():void
		{
			for each(var element:SoundChannel in channels)
			{
				element.stop();
			}
		}
		
		public function removeMe():void 
		{
			for each (var element:Object in sounds) 
			{
				element = null;				
			}
			for each (var element2:Object in channels) 
			{
				element2 = null;				
			}
			sounds = null;
			channels = null;			
		}
		
		private var channels:Dictionary;
		private var sounds:Dictionary;
	}
}